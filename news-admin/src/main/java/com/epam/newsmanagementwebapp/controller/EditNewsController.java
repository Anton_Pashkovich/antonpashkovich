package com.epam.newsmanagementwebapp.controller;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import javax.persistence.OptimisticLockException;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;

import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.valueobject.NewsVO;
import com.epam.newsmanagementwebapp.service.INewsManagementService;

@Controller
public class EditNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNews(
			@ModelAttribute(value = "newsTO") NewsTO news,
			@RequestParam(value = "authorId") Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedTags,
			@RequestParam(value = "date") String inputDate) {
		Date date = null;
		try {
			date = parseDate(inputDate, new SimpleDateFormat("MM/dd/yyyy"));
		} catch (ParseException e) {
			return "error";
		}
		news.setCreationDate(date);
		news.setModificationDate(date);
		Long newsId = null;
		try {
			newsId = newsManagementService
					.addNews(news, authorId, selectedTags);
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/news/" + newsId + "/view";
	}

	@RequestMapping("news/{newsId}/edit")
	public String goToEditNewsPage(Model model,
			@PathVariable(value = "newsId") Long newsId, @RequestParam(value = "lockMessage", required = false) String erorMessage) {
		try {
			if (erorMessage != null) {
				model.addAttribute("lockMessage", " ");
			}
			NewsVO news = newsManagementService.getSingleNews(newsId);
			model.addAttribute("newsTO", news.getNews());
			model.addAttribute("newsAuthor", news.getAuthor());
			model.addAttribute("newsTags", news.getTagIdList());
			model.addAttribute("authorsList",
					newsManagementService.getActualAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (Exception e) {
			return "error";
		}
		return "editNews";
	}

	@RequestMapping("/news/update")
	public String updateNews(Model model,
			@ModelAttribute(value = "newsTO") NewsTO news,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedTags,
			@RequestParam(value = "date") String inputDate,
			@RequestParam(value = "creation") String creation,
			@RequestParam(value = "version") Long version) {
		try {
			Date modificationDate = null;
			Date creationDate = null;
			modificationDate = parseDate(inputDate, new SimpleDateFormat(
					"MM/dd/yyyy"));
			creationDate = parseDate(creation, new SimpleDateFormat(
					"yyyy-MM-dd"));
			news.setCreationDate(creationDate);
			news.setModificationDate(modificationDate);
			news.setVersion(version);
			newsManagementService.editNews(news, authorId, selectedTags);
		} catch (ParseException e) {
			return "error";			
		} catch (OptimisticLockException e) {
			model.addAttribute("lockMessage", "error");
			return "redirect:/news/" + news.getId() + "/edit";
		}
		catch (Exception e) {
			return "error";
		}
		return "redirect:/news/" + news.getId() + "/view";
	}

	private Date parseDate(String inputDate, SimpleDateFormat formatter)
			throws ParseException {
		return formatter.parse(inputDate);
	}
}
