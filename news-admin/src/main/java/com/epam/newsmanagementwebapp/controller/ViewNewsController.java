package com.epam.newsmanagementwebapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.util.Filter;

@Controller
@SessionAttributes("filter")
public class ViewNewsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping("/news/{newsId}/view")
	public String viewNews(@PathVariable(value = "newsId") Long newsId,
			Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		try {
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (Exception e) {
			return "error";
		}
		return "view";
	}

	@RequestMapping(value = "/news/{newsId}/postComment", method = RequestMethod.POST)
	public String postComment(HttpServletRequest request,
			@PathVariable(value = "newsId") Long newsId,
			@ModelAttribute("commentText") String text, Model model) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		Date date = new Date();
		try {
			newsManagementService.addComment(new CommentTO(newsId, text, date));
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/news/{newsId}/view";
	}

	@RequestMapping("/news/{newsId}/deleteComment/{commentId}")
	public String deleteComment(HttpServletRequest request,
			@PathVariable(value = "newsId") Long newsId,
			@PathVariable(value = "commentId") Long commentId, Model model) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		try {
			newsManagementService.deleteComment(commentId);
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/news/{newsId}/view";
	}
}
