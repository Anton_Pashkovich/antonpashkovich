package com.epam.newsmanagementwebapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.entity.valueobject.NewsVO;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.util.Filter;

@Controller
@SessionAttributes({ "filter", "pageNum" })
public class NewsAdminController {

	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping("/")
	public String main(Model model) {
		return "forward:/news";
	}

	@RequestMapping("/news")
	public String index(Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		Long pageNum = (Long) request.getSession().getAttribute("pageNum");
		if (pageNum == null) {
			pageNum = 1L;
		}
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, pageNum));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
			model.addAttribute("pagesCnt",
					newsManagementService.getPagesCount(filter));
		} catch (ServiceException e) {
			return "error";
		}
		model.addAttribute("pageNum", pageNum);
		return "index";
	}

	@RequestMapping("/news/byFilter")
	public String setFilter(
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedIdTags,
			Model model) {
		Filter filter = new Filter();
		if (authorId != null) {
			filter.setAuthor(authorId);
		}
		if (selectedIdTags != null) {
			List<Long> selectedTagList = new ArrayList<Long>();
			for (Long tagId : selectedIdTags) {
				selectedTagList.add(tagId);
			}
			filter.setTags(selectedTagList);
		}
		model.addAttribute("pageNum", 1L);
		model.addAttribute("filter", filter);
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, 1L));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:";
	}

	@RequestMapping("news/reset")
	public String resetFilter(Model model) {
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", 1L);
		return "redirect:";
	}

	@RequestMapping("news/page/{pageNum}")
	public String setPage(@PathVariable(value = "pageNum") Long pageNum,
			Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		model.addAttribute("pageNum", pageNum);
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, pageNum));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
			model.addAttribute("pagesCnt",
					newsManagementService.getPagesCount(filter));
		} catch (ServiceException e) {
			return "error";
		}
		return "index";
	}

	@RequestMapping("/news/{newsId}/view")
	public String newsPage(@PathVariable(value = "newsId") Long newsId,
			Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		try {
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (ServiceException e) {
			return "error";
		}
		return "view";
	}

	@RequestMapping(value = "/news/{newsId}/postComment", method = RequestMethod.POST)
	public String postComment(HttpServletRequest request,
			@PathVariable(value = "newsId") Long newsId,
			@ModelAttribute("commentText") String text, Model model) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		Date date = new Date();
		try {
			newsManagementService.addComment(new CommentTO(newsId, text, date));
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/news/{newsId}/view";
	}

	@RequestMapping("/news/{newsId}/deleteComment/{commentId}")
	public String deleteComment(HttpServletRequest request,
			@PathVariable(value = "newsId") Long newsId,
			@PathVariable(value = "commentId") Long commentId, Model model) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		try {
			newsManagementService.deleteComment(commentId);
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/news/{newsId}/view";
	}

	@RequestMapping("/news/delete")
	public String deleteNews(
			@RequestParam(value = "selectedNews") List<Long> selectedNews) {
		try {
			newsManagementService.deleteNews(selectedNews);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/news";
	}

	@RequestMapping("/addNewsPage")
	public String addNewsPage(Model model) {
		NewsTO newsTO = new NewsTO();
		newsTO.setCreationDate(new Date());
		model.addAttribute("newsTO", newsTO);
		try {
			model.addAttribute("authorsList",
					newsManagementService.getActualAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (ServiceException e) {
			return "error";
		}
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", 1L);
		return "addNews";
	}

	@RequestMapping(value = "/addNews", method = RequestMethod.POST)
	public String addNews(
			@ModelAttribute(value = "newsTO") NewsTO news,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedTags) {
		List<Long> selectedTagList = new ArrayList<Long>();
		if (selectedTags != null) {
			for (Long tagId : selectedTags) {
				selectedTagList.add(tagId);
			}
		}
		Long newsId = null;
		try {
			newsId = newsManagementService.addNews(news, authorId,
					selectedTagList);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/news/" + newsId + "/view";
	}

	@RequestMapping("/editTagsPage")
	public String editTagsForm(Model model) {
		model.addAttribute("newTag", new TagTO());
		try {
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (ServiceException e) {
			return "error";
		}
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", 1L);
		return "editTags";
	}

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(@ModelAttribute(value = "newTag") TagTO newTag) {
		try {
			newsManagementService.addNewTag(newTag);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/editTagsPage";
	}

	@RequestMapping("/deleteTag/{tagId}")
	public String deleteTag(@PathVariable(value = "tagId") Long tagId) {
		try {
			newsManagementService.deleteTag(tagId);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/editTagsPage";
	}

	@RequestMapping(value = "/updateTag/{tagId}", method = RequestMethod.POST)
	public String updateTag(@PathVariable(value = "tagId") Long tagId,
			@ModelAttribute(value = "tagName") String tagName) {
		try {
			newsManagementService.updateTag(new TagTO(tagId, tagName));
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/editTagsPage";
	}

	@RequestMapping("/editAuthorsPage")
	public String editAuthorsForm(Model model) {
		model.addAttribute("newAuthor", new AuthorTO());
		try {
			model.addAttribute("authorsList",
					newsManagementService.getActualAuthors());
		} catch (ServiceException e) {
			return "error";
		}
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", 1L);
		return "editAuthors";
	}

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(
			@ModelAttribute(value = "newAuthor") AuthorTO newAuthor) {
		try {
			newsManagementService.addNewsAuthor(newAuthor);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/editAuthorsPage";
	}

	@RequestMapping("/expireAuthor/{authorId}")
	public String expireAuthor(@PathVariable(value = "authorId") Long authorId) {
		try {
			newsManagementService.expireAuthor(authorId);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/editAuthorsPage";
	}

	@RequestMapping("/updateAuthor/{authorId}")
	public String updateAuthor(@PathVariable(value = "authorId") Long authorId,
			@ModelAttribute(value = "authorName") String authorName) {
		try {
			newsManagementService.updateAuthor(new AuthorTO(authorId,
					authorName));
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/editAuthorsPage";
	}

	@RequestMapping("news/{newsId}/edit")
	public String editNewsPage(Model model,
			@PathVariable(value = "newsId") Long newsId) {
		try {
			NewsVO news = newsManagementService.getSingleNews(newsId);
			model.addAttribute("newsTO", news.getNews());
			model.addAttribute("newsAuthor", news.getAuthor());
			model.addAttribute("newsTags", news.getTagIdList());
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (ServiceException e) {
			return "error";
		}
		return "editNews";
	}

	@RequestMapping("/news/update")
	public String updateNews(
			@ModelAttribute(value = "newsTO") NewsTO news,
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedTags) {
		List<Long> selectedTagList = new ArrayList<Long>();
		if (selectedTags != null) {
			for (Long tagId : selectedTags) {
				selectedTagList.add(tagId);
			}
		}
		try {
			newsManagementService.editNews(news, authorId, selectedTagList);
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/news/" + news.getId() + "/view";
	}
}