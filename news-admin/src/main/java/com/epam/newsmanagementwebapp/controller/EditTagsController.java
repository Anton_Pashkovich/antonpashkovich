package com.epam.newsmanagementwebapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.service.INewsManagementService;

@Controller
public class EditTagsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping(value = "/addTag", method = RequestMethod.POST)
	public String addTag(@ModelAttribute(value = "newTag") TagTO newTag) {
		try {
			newsManagementService.addNewTag(newTag);
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/editTagsPage";
	}

	@RequestMapping("/deleteTag/{tagId}")
	public String deleteTag(@PathVariable(value = "tagId") Long tagId) {
		try {
			newsManagementService.deleteTag(tagId);
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/editTagsPage";
	}

	@RequestMapping(value = "/updateTag/{tagId}", method = RequestMethod.POST)
	public String updateTag(@PathVariable(value = "tagId") Long tagId,
			@ModelAttribute(value = "tagName") String tagName) {
		try {
			newsManagementService.updateTag(new TagTO(tagId, tagName));
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/editTagsPage";
	}
}
