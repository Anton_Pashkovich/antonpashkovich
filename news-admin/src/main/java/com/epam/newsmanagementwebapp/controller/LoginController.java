package com.epam.newsmanagementwebapp.controller;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;

@Controller
public class LoginController {

	@RequestMapping("/login")
	public String login(Model model, @RequestParam(value = "error", required = false) String erorMessage) {
		if (erorMessage != null) {
			model.addAttribute("error", " ");
		}
		return "login";
	}	
}