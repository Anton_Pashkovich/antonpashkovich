package com.epam.newsmanagementwebapp.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.service.INewsManagementService;

@Controller
public class EditAuthorsController {

	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping(value = "/addAuthor", method = RequestMethod.POST)
	public String addAuthor(
			@ModelAttribute(value = "newAuthor") AuthorTO newAuthor) {
		try {
			newsManagementService.addNewsAuthor(newAuthor);
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/editAuthorsPage";
	}

	@RequestMapping("/expireAuthor/{authorId}")
	public String expireAuthor(@PathVariable(value = "authorId") Long authorId) {
		try {
			newsManagementService.expireAuthor(authorId);
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/editAuthorsPage";
	}

	@RequestMapping("/updateAuthor/{authorId}")
	public String updateAuthor(@PathVariable(value = "authorId") Long authorId,
			@ModelAttribute(value = "authorName") String authorName) {
		try {
			newsManagementService.updateAuthor(new AuthorTO(authorId,
					authorName));
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/editAuthorsPage";
	}
}
