package com.epam.newsmanagementwebapp.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.util.Filter;

@Controller
@SessionAttributes({ "filter", "pageNum" })
public class NewsListController {

	@Autowired
	private INewsManagementService newsManagementService;
	private static final Long defaultPageNum = 1L;

	@RequestMapping("/")
	public String goToNewsList(Model model) {
		return "forward:/news";
	}

	@RequestMapping("/news/byFilter")
	public String filterNews(
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedIdTags,
			Model model) {
		Filter filter = new Filter();
		filter.setAuthor(authorId);
		filter.setTags(selectedIdTags);
		model.addAttribute("pageNum", defaultPageNum);
		model.addAttribute("filter", filter);
		try {
		model.addAttribute("newsList",
				newsManagementService.getNewsByFilter(filter, defaultPageNum));
		model.addAttribute("authorsList", newsManagementService.getAllAuthors());
		model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/news";
	}

	@RequestMapping("news/reset")
	public String resetFilter(Model model) {
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", defaultPageNum);
		return "redirect:/news";
	}

	@RequestMapping("news/page/{pageNum}")
	public String goToPage(@PathVariable(value = "pageNum") Long pageNum,
			Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		model.addAttribute("pageNum", pageNum);
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, pageNum));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
			model.addAttribute("pagesCnt",
					newsManagementService.getPagesCount(filter));
		} catch (Exception e) {
			return "error";
		}
		return "index";
	}

	@RequestMapping("/news/delete")
	public String deleteNews(
			@RequestParam(value = "selectedNews") List<Long> selectedNews) {
		try {
			newsManagementService.deleteNews(selectedNews);
		} catch (Exception e) {
			return "error";
		}
		return "redirect:/news";
	}
}