package com.epam.newsmanagementwebapp.controller;

import java.util.Date;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.util.Filter;

@Controller
@SessionAttributes({ "filter", "pageNum" })
public class MenuController {

	@Autowired
	private INewsManagementService newsManagementService;
	private static final Long defaultPageNum = 1L;

	@RequestMapping("/news")
	public String index(Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		Long pageNum = (Long) request.getSession().getAttribute("pageNum");
		if (pageNum == null) {
			pageNum = defaultPageNum;
		}
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, pageNum));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
			model.addAttribute("pagesCnt",
					newsManagementService.getPagesCount(filter));
			model.addAttribute("pageNum", pageNum);
		} catch (Exception e) {
			return "error";
		}
		return "index";
	}

	@RequestMapping("/addNewsPage")
	public String goToAddNewsPage(Model model) {
		NewsTO newsTO = new NewsTO();
		newsTO.setCreationDate(new Date());
		model.addAttribute("newsTO", newsTO);
		try {
			model.addAttribute("authorsList",
					newsManagementService.getActualAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (Exception e) {
			return "error";
		}
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", defaultPageNum);
		return "addNews";
	}

	@RequestMapping("/editTagsPage")
	public String goToeditTagsPage(Model model) {
		model.addAttribute("newTag", new TagTO());
		try {
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (Exception e) {
			return "error";
		}
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", defaultPageNum);
		return "editTags";
	}

	@RequestMapping("/editAuthorsPage")
	public String goToEditAuthorsPage(Model model) {
		model.addAttribute("newAuthor", new AuthorTO());
		try {
			model.addAttribute("authorsList",
					newsManagementService.getActualAuthors());
		} catch (Exception e) {
			return "error";
		}
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", defaultPageNum);
		return "editAuthors";
	}
}
