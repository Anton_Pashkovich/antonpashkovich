$('.deleteButton').click(function(e) {
	if (confirm($('#confirmDelete').attr('value'))) {
		return true;
	} else {
		return false;
	}
});

$('.submitButton').click(function(e) {
	var comment = $("textarea[name='commentText']").val();
	if (comment == null || comment == "") {
		alert($('#emptyComment').attr('value'));
		return false;
	}
});