function checkForm() {
	var check = true;
	var message = "";

	var title = $("input[name='title']").val();
	if (title == null || title == "") {
		message += $('#emptyTitle').attr('value');
		check = false;
	}

	var inputDate = $("input[name='date']").val();
	var value = $("input[name='date']").val();
	var pattern = new RegExp($('#pattern').attr('value'));
	if (!pattern.test(value)) {
		message += $('#incorrectDate').attr('value');
		check = false;
	} else {
		if ($('#locale').attr('value') == "ru") {
			parts = value.split('/');
			value = parts[1] + "/" + parts[0] + "/" + parts[2];
			$("input[name='date']").val(value);
		}
		date = new Date(value);
		today = new Date();
		if (date > today) {
			message += $('#dateOutOfRange').attr('value');
			check = false;
		}
	}
	
	var shortText = $("textarea[name='shortText']").val();
	if (shortText == null || shortText == "") {
		message += $('#emptyShort').attr('value');
		check = false;
	}
	
	var fullText = $("textarea[name='fullText']").val();
	if (fullText == null || fullText == "") {
		message += $('#emptyFull').attr('value');
		check = false;
	}
	
	if ($("select[name='authorId']").val() == null) {
		message += $('#emptyAuthor').attr('value');
		check = false;
	}
	
	if (!check) {
		$("input[name='date']").val(inputDate);
		alert(message);
	}
	return check;
}