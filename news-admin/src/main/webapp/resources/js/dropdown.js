$('label').click(function(e) {
	var checked = $(this).children('input').prop('checked');
	if (checked === false) {
		$(this).children('input').prop('checked', true);
	}
	else {
		$(this).children('input').prop('checked', false);
	}
});

$("input[name='selectedTags']").click(function(e) {
	var checked = $(this).prop('checked');
	if (checked === false) {
		$(this).prop('checked', true);
	}
	else {
		$(this).prop('checked', false);
	}
});