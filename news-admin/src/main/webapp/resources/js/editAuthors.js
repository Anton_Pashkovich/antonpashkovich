$('.author .edit').click(function(e) {
	$(this).hide();
	$(this).siblings('.update').show();
	$(this).parent('.editform').siblings('.expireform').children('.expire').show();
	$(this).parent('.editform').siblings('.cancel').show();
	$(this).siblings('.authorName').removeAttr("readonly");
	$(this).siblings('.authorName').css('background', '#ffffff');
	$(this).siblings('.authorName').css('color', '#000000');
	e.preventDefault();
});

$('.author .cancel').click(function(e) {
	$(this).hide();
	$(this).siblings('.expireform').children('.expire').hide();
	$(this).siblings('.editform').children('.update').hide();
	$(this).siblings('.editform').children('.edit').show();
	$(this).siblings('.editform').children('.authorName').attr('readonly', true);
	$(this).siblings('.editform').children('.authorName').css('background', '#eaeaea');
	$(this).siblings('.editform').children('.authorName').css('color', '#787878');
	var name = $(this).siblings('.editform').children('.authorName').attr('value');
	$(this).siblings('.editform').children('.authorName').val(name);
	e.preventDefault();
});

$('.author .expire').click(function(e) {
	if (confirm($('#confirmExpire').attr('value'))) {
		return true;
	} else {
		return false;
	}
});

$('.author .update').click(function(e) {
	var name = $(this).siblings('.authorName').val();
	if (name == null || name == "") {
		alert($('#emptyAuthorName').attr('value'));
        return false;
	}
	if (confirm($('#confirmUpdate').attr('value'))) {
		return true;
	} else {
		return false;
	}
});

$('.author .submitLink').click(function(e) {
	var name = document.forms["author-form"]["name"].value;
	if (name == null || name == "") {
		alert($('#emptyAuthorName').attr('value'));
        return false;
	}
	if (confirm($('#confirmCreate').attr('value'))) {
		return true;
	} else {
		return false;
	}
});