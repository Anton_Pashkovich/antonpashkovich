$('.tag .edit').click(function(e) {
	$(this).hide();
	$(this).siblings('.update').show();
	$(this).parent('.editform').siblings('.deleteform').children('.delete').show();
	$(this).parent('.editform').siblings('.cancel').show();
	$(this).siblings('.tagName').removeAttr("readonly");
	$(this).siblings('.tagName').css('background', '#ffffff');
	$(this).siblings('.tagName').css('color', '#000000');
	e.preventDefault();
});

$('.tag .cancel').click(function(e) {
	$(this).hide();
	$(this).siblings('.deleteform').children('.delete').hide();
	$(this).siblings('.editform').children('.update').hide();
	$(this).siblings('.editform').children('.edit').show();
	$(this).siblings('.editform').children('.tagName').attr('readonly', true);
	$(this).siblings('.editform').children('.tagName').css('background', '#eaeaea');
	$(this).siblings('.editform').children('.tagName').css('color', '#787878');
	var name = $(this).siblings('.editform').children('.tagName').attr('value');
	$(this).siblings('.editform').children('.tagName').val(name);
	e.preventDefault();
});

$('.tag .delete').click(function(e) {
	if (confirm($('#confirmDelete').attr('value'))) {
		return true;
	} else {
		return false;
	}
});

$('.tag .update').click(function(e) {
	var name = $(this).siblings('.tagName').val();
	if (name == null || name == "") {
		alert($('#emptyTagName').attr('value'));
        return false;
	}
	if (confirm($('#confirmUpdate').attr('value'))) {
		return true;
	} else {
		return false;
	}
});

$('.tag .submitLink').click(function(e) {
	var name = document.forms["tag-form"]["name"].value;
	if (name == null || name == "") {
		alert($('#emptyTagName').attr('value'));
        return false;
	}
	if (confirm($('#confirmCreate').attr('value'))) {
		return true;
	} else {
		return false;
	}
});