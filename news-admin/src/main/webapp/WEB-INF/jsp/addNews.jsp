<%@ page language="java" contentType="text/html; UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/editNews.css" />
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
</head>
<form:form action="${pageContext.request.contextPath}/addNews"
	modelAttribute="newsTO" method="POST" onsubmit="return checkForm();">
	<div class="content">
		<div class="newsField">
			<span><spring:message code="label.title" /></span>
			<form:input path="title" type="text" name="title" maxlength="80" />
		</div>
		<div class="newsField">
			<span><spring:message code="label.date" /></span> <input type="text"
				name="date"
				value="" placeholder="<spring:message code="label.date.placeholder" />">
		</div>
		<div class="newsField">
			<span><spring:message code="label.brief" /></span>
			<form:textarea path="shortText" maxlength="200" name="brief" ></form:textarea>
		</div>
		<div class="newsField">
			<span><spring:message code="label.content" /></span>
			<form:textarea path="fullText" maxlength="2000" name="content" ></form:textarea>
		</div>
		<div class="dropdowns">
			<div class="multiselect">
				<select name="authorId" size="1">
					<option selected="selected" disabled value=""><spring:message
							code="label.select.author" /></option>
					<c:forEach var="author" items="${authorsList}">
						<option style="color: black;" value="${author.id}">
							<c:out value="${author.name}" />
						</option>
					</c:forEach>
				</select>
			</div>
			<div class="multiselect">
				<div class="select_box" onclick="showCheckboxes()">
					<select>
						<option selected="selected" value="false"><spring:message
								code="label.select.tags" /></option>
					</select>
					<div class="over_select" id="over_select"></div>
				</div>
				<div id="checkboxes">
					<c:forEach var="tag" items="${tagsList}">
						<label for="${tag.name}"> <c:set var="checked" value="no" />
							<input type="checkbox" name="selectedTags" value="${tag.id}" />
							<span><c:out value="${tag.name}" /></span>
						</label>
					</c:forEach>
				</div>
			</div>
		</div>
		<div class="savebutton">
			<input type="submit" value="<spring:message code="label.save" />">
		</div>
	</div>
</form:form>

<input type="hidden" id="pattern" value="<spring:message code="label.date.pattern" />">
<input type="hidden" id="locale" value="<spring:message code="label.locale" />">
<input type="hidden" id="emptyTitle" value="<spring:message code="label.empty.title" />">
<input type="hidden" id="emptyShort" value="<spring:message code="label.empty.shortText" />">
<input type="hidden" id="emptyFull" value="<spring:message code="label.epmty.fullText" />">
<input type="hidden" id="emptyAuthor" value="<spring:message code="label.epmty.author" />">
<input type="hidden" id="incorrectDate" value="<spring:message code="label.incorrect.date" />">
<input type="hidden" id="dateOutOfRange" value="<spring:message code="label.date.out.of.range" />">

<script src="${pageContext.request.contextPath}/resources/js/dropdown.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/checkAddForm.js"></script>

<script src="${pageContext.request.contextPath}/resources/js/showCheckboxes.js"></script>