<%@ page language="java" contentType="text/html; UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/editAuthors.css" />
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
</head>
<div class="content">
	<ul class="authorsList">
		<c:forEach var="author" items="${authorsList}">
			<li class="author"><span class="authorword"><spring:message code="label.author" /></span>
			<form class="editform" action="${pageContext.request.contextPath}/updateAuthor/${author.id}" method="POST">
				<input type="text" name="authorName" class="authorName" value="${author.name}" readonly>
				<input type="button" class="edit" value="<spring:message code="label.edit" />">
				<input type="submit" class="update" value="<spring:message code="label.update" />">
			</form>
			<form class="expireform" action="${pageContext.request.contextPath}/expireAuthor/${author.id}">
				<input type="submit" class="expire" value="<spring:message code="label.expire" />">
			</form>
			<input type="button" class="cancel" value="<spring:message code="label.cancel" />">
			</li>
		</c:forEach>
	</ul>
	<div class="author" id="div-author-form">
		<form:form id="author-form"
			action="${pageContext.request.contextPath}/addAuthor"
			modelAttribute="newAuthor" method="POST">
			<span class="add-author-word"><spring:message
					code="label.add.author" /></span>
			<form:input path="name" class="add-author-input" type="text" />
			<input type="submit" class="submitLink"
				value="<spring:message code="label.save" />" >
		</form:form>
	</div>
</div>
<input type="hidden" id="confirmExpire" value="<spring:message code="label.confirm.expire.author" />">
<input type="hidden" id="confirmUpdate" value="<spring:message code="label.confirm.update.author" />">
<input type="hidden" id="emptyAuthorName" value="<spring:message code="label.author.name.empty" />">
<input type="hidden" id="confirmCreate" value="<spring:message code="label.confirm.create.author" />">

<script src="${pageContext.request.contextPath}/resources/js/editAuthors.js"></script>