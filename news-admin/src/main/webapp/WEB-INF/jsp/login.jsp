<%@ page language="java" contentType="text/html; UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form" %>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/login.css" />
</head>

<div class="loginbody">
<c:url value="/j_spring_security_check" var="loginUrl" />
	<form method="POST"
		action="${loginUrl}">
		<input type="hidden" name="${_csrf.parameterName}" value="${_csrf.token}" />
		<table>
			<tbody>
				<tr>
					<td><spring:message code="label.login" /></td>
					<td><input type="text" name="j_username" value="" required
						autofocus></td>
				</tr>
				<tr>
					<td><spring:message code="label.password" /></td>
					<td><input type="password" name="j_password" required></td>
				</tr>
				<tr>
					<td colspan="2">
						<c:if test="${not empty error}">
							<span class="error"><spring:message code="label.incorrect.login.data" /></span>
						</c:if>
						<button type="submit"><spring:message code="label.button.login" /></button></td>
				</tr>
			</tbody>
		</table>
	</form>
</div>