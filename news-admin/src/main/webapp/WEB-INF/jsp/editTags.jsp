<%@ page language="java" contentType="text/html; UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="form" uri="http://www.springframework.org/tags/form"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/editTags.css" />
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
</head>
<div class="content">
	<ul class="tagsList">
		<c:forEach var="tag" items="${tagsList}">
			<li class="tag"><span class="tagword"><spring:message code="label.tag" /></span>
			<form class="editform" action="${pageContext.request.contextPath}/updateTag/${tag.id}" method="POST">
				<input type="text" class="tagName" name="tagName" value="${tag.name}" readonly>
				<input type="button" class="edit" value="<spring:message code="label.edit" />">
				<input type="submit" class="update" value="<spring:message code="label.update" />">
			</form>
			<form class="deleteform" action="${pageContext.request.contextPath}/deleteTag/${tag.id}">
				<input type="submit" class="delete" value="<spring:message code="label.delete" />">
			</form>
			<input type="button" class="cancel" value="<spring:message code="label.cancel" />">
			</li>
		</c:forEach>
	</ul>
	<div class="tag" id="div-tag-form">
		<form:form id="tag-form"
			action="${pageContext.request.contextPath}/addTag"
			modelAttribute="newTag" method="POST">
			<span class="add-tag-word"><spring:message
					code="label.add.tag" /></span>
			<form:input path="name" class="add-tag-input" type="text" />
			<input type="submit" class="submitLink"
				value="<spring:message code="label.save" />" >
		</form:form>
	</div>
</div>

<input type="hidden" id="confirmDelete" value="<spring:message code="label.confirm.delete.tag" />">
<input type="hidden" id="confirmUpdate" value="<spring:message code="label.confirm.update.author" />">
<input type="hidden" id="emptyTagName" value="<spring:message code="label.tag.name.empty" />">
<input type="hidden" id="confirmCreate" value="<spring:message code="label.confirm.create.tag" />">

<script src="${pageContext.request.contextPath}/resources/js/editTags.js"></script>