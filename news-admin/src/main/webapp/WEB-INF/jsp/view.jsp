<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/view-news.css" />
<script src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
</head>
<div class="content">
	<div class="links">
		<c:url var="index" value="/news" />
		<a href="${index}"><spring:message code="label.button.back" /></a>
	</div>
	<div class="news">
		<div class="news_title">
			<c:out value="${news.news.title}" />
		</div>
		<div class="author_date">
			<div class="author">
				<c:out value="( by ${news.author.name} )" />
			</div>
			<div class="date">
				<fmt:formatDate value="${news.news.modificationDate}" />
			</div>
		</div>

		<div class="text">
			<c:out value="${news.news.fullText}" />
		</div>
		<c:forEach var="comment" items="${news.comments}">
			<div class="comments">
				<span> <fmt:formatDate type="both"
						value="${comment.creationDate}" />
				</span>
				<div class="commentText">
					<c:out value="${comment.commentText}" />
					<div class="deleteButton">
						<a
							href="<spring:url value="/news/${news.news.id}/deleteComment/${comment.id}" />">
							<button>x</button>
						</a>
					</div>
				</div>
			</div>
		</c:forEach>
		<div class="postComment">
			<form
				action="${pageContext.request.contextPath}/news/${news.news.id}/postComment"
				method="POST">
				<textarea rows="5" cols="45" name="commentText"></textarea>
				<input type="submit" class="submitButton"
					value="<spring:message code="label.button.postcomment" />">
			</form>
		</div>
	</div>

	<c:if test="${not empty nextId}">
		<div class="next">
			<c:url var="view" value="/news/${nextId}/view" />
			<a href="${view}"><spring:message code="label.button.next" /></a>
		</div>
	</c:if>

	<c:if test="${not empty prevId}">
		<div class="previous">
			<c:url var="view" value="/news/${prevId}/view" />
			<a href="${view}"><spring:message code="label.button.previous" /></a>
		</div>
	</c:if>
</div>

<input type="hidden" id="confirmDelete" value="<spring:message code="label.confirm.delete.comment" />">
<input type="hidden" id="emptyComment" value="<spring:message code="label.empty.comment" />">

<script src="${pageContext.request.contextPath}/resources/js/viewNews.js"></script>