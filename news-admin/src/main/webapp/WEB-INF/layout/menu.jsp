<%@ page language="java" contentType="text/html; charset=UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<div class="menu">
	<ul>
		<li><a href="${pageContext.request.contextPath}/news"><spring:message
					code="label.news.list" /></a></li>
		<li><a href="${pageContext.request.contextPath}/addNewsPage"><spring:message
					code="label.add.news" /></a></li>
		<li><a href="${pageContext.request.contextPath}/editAuthorsPage"><spring:message
					code="label.add.update.authors" /></a></li>
		<li><a href="${pageContext.request.contextPath}/editTagsPage"><spring:message
					code="label.add.update.tags" /></a></li>
	</ul>
</div>
