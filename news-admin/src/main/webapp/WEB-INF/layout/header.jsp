<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib uri="http://www.springframework.org/security/tags" prefix="security" %>
<div class="header">
	<div class="header_title">News Portal - Administration</div>
	<security:authorize access="isAuthenticated()">
	<div class="logout">
		<a href="<spring:url value="/logout" />">
    		<button><spring:message code="label.logout" /></button>
		</a>
	</div>
	</security:authorize>
	<div class="localization">
		<span> <c:url var="index" value="/news/changeLocale" /> <a
			href="?lang=en">EN</a> <a href="?lang=ru">RU</a>
		</span>
	</div>
</div>