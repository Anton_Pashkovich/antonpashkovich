package com.epam.newsmanagementwebapp.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagementwebapp.dao.ICommentDao;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.service.ICommentService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestAppContext.xml"})
public class CommentServiceTest {

	@Mock
	private ICommentDao commentDao;

	@InjectMocks
	@Autowired
	private ICommentService commentService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAddComment() throws Exception {
		CommentTO comment = new CommentTO();
		comment.setNewsId(1L);
		comment.setCommentText("text");
		comment.setCreationDate(Timestamp
				.valueOf("2007-09-23 10:10:10"));
		Long expectedId = 1L;
		when(commentDao.create(comment)).thenReturn(expectedId);
		Long actualId = commentService.addComment(comment);
		verify(commentDao, times(1)).create(comment);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void testDeleteComment() throws Exception {
		Long commentId = 1L;
		commentService.deleteComment(commentId);
		verify(commentDao, times(1)).delete(commentId);
	}
	
	@Test
	public void testGetNewsComments() throws Exception {
		Long newsId = 1L;
		List<CommentTO> expectedList = new ArrayList<CommentTO>();
		expectedList.add(new CommentTO(1L, 1L, "text1", Timestamp
				.valueOf("2007-09-23 10:10:10")));
		expectedList.add(new CommentTO(2L, 1L, "text1", Timestamp
				.valueOf("2007-09-23 10:10:10")));
		when(commentDao.getNewsComments(newsId)).thenReturn(expectedList);
		List<CommentTO> actualList = commentService.getNewsComments(newsId);
		verify(commentDao, times(1)).getNewsComments(newsId);
		assertEquals(expectedList, actualList);
	}
}