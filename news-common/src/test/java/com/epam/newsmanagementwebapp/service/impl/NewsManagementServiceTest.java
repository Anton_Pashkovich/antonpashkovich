package com.epam.newsmanagementwebapp.service.impl;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.inOrder;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InOrder;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.entity.valueobject.NewsVO;
import com.epam.newsmanagementwebapp.service.IAuthorService;
import com.epam.newsmanagementwebapp.service.ICommentService;
import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.service.INewsService;
import com.epam.newsmanagementwebapp.service.ITagService;
import com.epam.newsmanagementwebapp.service.impl.NewsManagementService;

@RunWith(MockitoJUnitRunner.class)
public class NewsManagementServiceTest {

	@InjectMocks
	private INewsManagementService newsManagementService = new NewsManagementService();

	@Mock
	private IAuthorService authorService;

	@Mock
	private INewsService newsService;

	@Mock
	private ITagService tagService;

	@Mock
	private ICommentService commentService;

//	@Test
//	public void testAddNews() throws Exception {
//		Long newsId = 1L;
//		String newsShortText = "ShortText";
//		String newsFullText = "FullText";
//		String newsTitle = "Title";
//		Date newsCreationDate = new Date(11111L);
//		Date newsModificationDate = new Date(22222L);
//		NewsTO newsTO = new NewsTO();
//		newsTO.setCreationDate(newsCreationDate);
//		newsTO.setFullText(newsFullText);
//		newsTO.setModificationDate(newsModificationDate);
//		newsTO.setShortText(newsShortText);
//		newsTO.setTitle(newsTitle);
//
//		when(newsService.addNews(newsTO)).thenReturn(newsId);
//
//		Long authorId = 1L;
//		String authorName = "AuthorName";
//		AuthorTO author = new AuthorTO();
//		author.setId(authorId);
//		author.setName(authorName);
//
//		doNothing().when(authorService).bindAuthorToNews(author.getId(),
//				newsTO.getId());
//
//		Long firstTagId = 1L;
//		String firstTagName = "Name1";
//		Long secondTagId = 2L;
//		String secondTagName = "Name2";
//		TagTO firstTag = new TagTO();
//		firstTag.setId(firstTagId);
//		firstTag.setName(firstTagName);
//		TagTO secondTag = new TagTO();
//		secondTag.setId(secondTagId);
//		secondTag.setName(secondTagName);
//		List<TagTO> tagList = new ArrayList<TagTO>();
//		tagList.add(firstTag);
//		tagList.add(secondTag);
//
//		doNothing().when(tagService).bindTagToNews(tagList.get(0).getId(),
//				newsId);
//		doNothing().when(tagService).bindTagToNews(tagList.get(1).getId(),
//				newsId);
//
//		NewsVO newsDTO = new NewsVO();
//		newsDTO.setAuthor(author);
//		newsDTO.setNews(newsTO);
//		newsDTO.setTags(tagList);
//
//		newsManagementService.addNews(newsTO, author, tagList);
//
//		InOrder inOrder = inOrder(newsService, tagService, authorService);
//		inOrder.verify(newsService).addNews(newsTO);
//		inOrder.verify(authorService).bindAuthorToNews(author.getId(),
//				newsTO.getId());
//		inOrder.verify(tagService)
//				.bindTagToNews(tagList.get(0).getId(), newsId);
//		inOrder.verify(tagService)
//				.bindTagToNews(tagList.get(1).getId(), newsId);
//	}

	@Test
	public void testDeleteNews() throws Exception {
		Long newsId = 1L;
		doNothing().when(newsService).deleteNews(newsId);
		newsManagementService.deleteNews(newsId);
		verify(newsService, times(1)).deleteNews(newsId);
	}

	@Test
	public void testEditNews() throws Exception {
		Long newsId = 1L;
		String newsShortText = "updShortText";
		String newsFullText = "updFullText";
		String newsTitle = "updTitle";
		Date newsCreationDate = new Date(11111L);
		Date newsModificationDate = new Date(22222L);
		NewsTO news = new NewsTO();
		news.setId(newsId);
		news.setCreationDate(newsCreationDate);
		news.setFullText(newsFullText);
		news.setModificationDate(newsModificationDate);
		news.setShortText(newsShortText);
		news.setTitle(newsTitle);

		Long authorId = 1L;
		String authorName = "AuthorName";
		AuthorTO author = new AuthorTO();
		author.setId(authorId);
		author.setName(authorName);

		Long firstTagId = 1L;
		String firstTagName = "Name1";
		Long secondTagId = 2L;
		String secondTagName = "Name2";
		TagTO firstTag = new TagTO();
		firstTag.setId(firstTagId);
		firstTag.setName(firstTagName);
		TagTO secondTag = new TagTO();
		secondTag.setId(secondTagId);
		secondTag.setName(secondTagName);
		List<TagTO> tagList = new ArrayList<TagTO>();
		tagList.add(firstTag);
		tagList.add(secondTag);

//		newsManagementService.editNews(news, author, tagList);

		InOrder inOrder = inOrder(newsService, tagService, authorService);
		inOrder.verify(newsService).editNews(news);
		inOrder.verify(authorService).unbindAuthorFromNews(news.getId());
		inOrder.verify(authorService).bindAuthorToNews(author.getId(),
				news.getId());
		inOrder.verify(tagService).unbindAllTags(news.getId());
		inOrder.verify(tagService)
				.bindTagToNews(tagList.get(0).getId(), newsId);
		inOrder.verify(tagService)
				.bindTagToNews(tagList.get(1).getId(), newsId);
	}

	@Test
	public void testGetAllNews() throws Exception {
		List<NewsVO> expectedNews = getExpectedNewsVOList();
		when(newsService.getAllNews()).thenReturn(getExpectedNewsList());
		when(authorService.getNewsAuthor(1L)).thenReturn(getExpectedAuthorTO());
		when(authorService.getNewsAuthor(2L)).thenReturn(getExpectedAuthorTO());
		when(tagService.getNewsTags(1L)).thenReturn(getExpectedTagTOList());
		when(tagService.getNewsTags(2L)).thenReturn(getExpectedTagTOList());
		when(commentService.getNewsComments(1L)).thenReturn(
				getExpectedCommentTOList());
		when(commentService.getNewsComments(2L)).thenReturn(
				getExpectedCommentTOList());
		List<NewsVO> actualNews = newsManagementService.getAllNews();
		assertNotNull(actualNews);
		assertEquals(expectedNews, actualNews);
	}

	@Test
	public void testGetSingleNews() throws Exception {
		Long newsId = 1L;
		when(newsService.getSingleNews(newsId)).thenReturn(getExpectedNews());
		when(authorService.getNewsAuthor(newsId)).thenReturn(
				getExpectedAuthorTO());
		when(tagService.getNewsTags(newsId)).thenReturn(getExpectedTagTOList());
		when(commentService.getNewsComments(newsId)).thenReturn(
				getExpectedCommentTOList());

		NewsVO expectedNews = new NewsVO();
		expectedNews.setAuthor(getExpectedAuthorTO());
		expectedNews.setComments(getExpectedCommentTOList());
		expectedNews.setNews(getExpectedNews());
		expectedNews.setTags(getExpectedTagTOList());

		NewsVO actualNews = newsManagementService.getSingleNews(newsId);
		assertEquals(expectedNews, actualNews);
	}

	public void testAddNewsAuthor() throws Exception {
		Long expectedId = 1L;
		AuthorTO author = new AuthorTO();
		author.setName("authorName");
		when(authorService.addNewAuthor(author)).thenReturn(expectedId);
		Long actualId = newsManagementService.addNewsAuthor(author);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void testAddComment() throws Exception {
		Long expectedId = 1L;
		CommentTO comment = new CommentTO();
		comment.setCreationDate(new Date(11111L));
		comment.setNewsId(1L);
		comment.setCommentText("text");
		when(commentService.addComment(comment)).thenReturn(expectedId);
		Long actualId = newsManagementService.addComment(comment);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void testDeleteComment() throws Exception {
		Long commentId = 1L;
		doNothing().when(commentService).deleteComment(commentId);
		newsManagementService.deleteComment(commentId);
		verify(commentService, times(1)).deleteComment(commentId);
	}

	private List<NewsVO> getExpectedNewsVOList() {
		NewsVO firstNewsVO = new NewsVO();
		firstNewsVO.setAuthor(getExpectedAuthorTO());
		firstNewsVO.setComments(getExpectedCommentTOList());
		firstNewsVO.setNews(getExpectedNews());
		firstNewsVO.setTags(getExpectedTagTOList());

		NewsVO secondNewsVO = new NewsVO();
		secondNewsVO.setAuthor(getExpectedAuthorTO());
		secondNewsVO.setComments(getExpectedCommentTOList());
		NewsTO news = getExpectedNews();
		news.setId(2L);
		secondNewsVO.setNews(news);
		secondNewsVO.setTags(getExpectedTagTOList());

		List<NewsVO> NewsVOList = new ArrayList<NewsVO>();
		NewsVOList.add(firstNewsVO);
		NewsVOList.add(secondNewsVO);
		return NewsVOList;
	}

	private List<NewsTO> getExpectedNewsList() {
		Long newsId1 = 1L;
		String newsShortText1 = "testShortText";
		String newsFullText1 = "testFullText";
		String newsTitle1 = "testTitle";
		Date newsCreationDate1 = new Date(11111L);
		Date newsModificationDate1 = new Date(22222L);
		NewsTO newsMessage1 = new NewsTO();
		newsMessage1.setId(newsId1);
		newsMessage1.setCreationDate(newsCreationDate1);
		newsMessage1.setFullText(newsFullText1);
		newsMessage1.setModificationDate(newsModificationDate1);
		newsMessage1.setShortText(newsShortText1);
		newsMessage1.setTitle(newsTitle1);

		Long newsId2 = 2L;
		String newsShortText2 = "testShortText";
		String newsFullText2 = "testFullText";
		String newsTitle2 = "testTitle";
		Date newsCreationDate2 = new Date(11111L);
		Date newsModificationDate2 = new Date(22222L);
		NewsTO newsMessage2 = new NewsTO();
		newsMessage2.setId(newsId2);
		newsMessage2.setCreationDate(newsCreationDate2);
		newsMessage2.setFullText(newsFullText2);
		newsMessage2.setModificationDate(newsModificationDate2);
		newsMessage2.setShortText(newsShortText2);
		newsMessage2.setTitle(newsTitle2);

		List<NewsTO> newsList = new ArrayList<NewsTO>();
		newsList.add(newsMessage1);
		newsList.add(newsMessage2);

		return newsList;
	}

	private NewsTO getExpectedNews() {
		Long newsId = 1L;
		String newsShortText = "testShortText";
		String newsFullText = "testFullText";
		String newsTitle = "testTitle";
		Date newsCreationDate = new Date(11111L);
		Date newsModificationDate = new Date(22222L);
		NewsTO newsMessage = new NewsTO();
		newsMessage.setId(newsId);
		newsMessage.setCreationDate(newsCreationDate);
		newsMessage.setFullText(newsFullText);
		newsMessage.setModificationDate(newsModificationDate);
		newsMessage.setShortText(newsShortText);
		newsMessage.setTitle(newsTitle);
		return newsMessage;
	}

	private AuthorTO getExpectedAuthorTO() {
		AuthorTO author = new AuthorTO();
		author.setId(1L);
		author.setName("authorTestName");
		return author;
	}

	private List<TagTO> getExpectedTagTOList() {
		TagTO firstTagTO = new TagTO();
		firstTagTO.setId(1L);
		firstTagTO.setName("tagName111");
		TagTO secondTagTO = new TagTO();
		secondTagTO.setId(2L);
		secondTagTO.setName("tagName222");
		List<TagTO> tagList = new ArrayList<TagTO>();
		tagList.add(firstTagTO);
		tagList.add(secondTagTO);
		return tagList;
	}

	private List<CommentTO> getExpectedCommentTOList() {
		Long firstCommentTOId = 1L;
		Long firstNewsId = 1L;
		String firstCommentTOText = "commentTestText";
		Date firstCommentTOCreationDate = new Date(11111L);
		Long secondCommentTOId = 2L;
		Long secondNewsId = 2L;
		String secondCommentTOText = "commentTestText2";
		Date secondCommentTOCreationDate = new Date(22222L);
		CommentTO firstCommentTO = new CommentTO();
		firstCommentTO.setCreationDate(firstCommentTOCreationDate);
		firstCommentTO.setId(firstCommentTOId);
		firstCommentTO.setNewsId(firstNewsId);
		firstCommentTO.setCommentText(firstCommentTOText);
		CommentTO secondCommentTO = new CommentTO();
		secondCommentTO.setCreationDate(secondCommentTOCreationDate);
		secondCommentTO.setId(secondCommentTOId);
		secondCommentTO.setNewsId(secondNewsId);
		secondCommentTO.setCommentText(secondCommentTOText);
		List<CommentTO> commentList = new ArrayList<CommentTO>();
		commentList.add(firstCommentTO);
		commentList.add(secondCommentTO);
		return commentList;
	}
}