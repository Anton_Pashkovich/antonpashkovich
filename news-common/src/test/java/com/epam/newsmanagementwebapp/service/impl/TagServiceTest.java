package com.epam.newsmanagementwebapp.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagementwebapp.dao.ITagDao;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.service.ITagService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestAppContext.xml"})
public class TagServiceTest {

	@Mock
	private ITagDao tagDao;

	@InjectMocks
	@Autowired
	private ITagService tagService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAddNewTag() throws Exception {
		TagTO tag = new TagTO();
		tag.setName("tagName");
		Long expectedId = 1L;
		when(tagDao.create(tag)).thenReturn(expectedId);
		Long actualId = tagService.addNewTag(tag);
		verify(tagDao, times(1)).create(tag);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void testBindTagToNews() throws Exception {
		Long tagId = 1L;
		Long newsId = 1L;
		tagService.bindTagToNews(tagId, newsId);
		verify(tagDao, times(1)).bindTagToNews(tagId, newsId);
	}
	
	@Test
	public void testUnbindAllTags() throws Exception {
		Long newsId = 1L;
		tagService.unbindAllTags(newsId);
		verify(tagDao, times(1)).unbindAllTags(newsId);
	}
	
	@Test
	public void testGetNewsTags() throws Exception {
		List<TagTO> expectedTags = new ArrayList<TagTO>();
		expectedTags.add(new TagTO(1L, "FirstTag"));
		expectedTags.add(new TagTO(1L, "SecondTag"));
		Long newsId = 1L;
		when(tagDao.getNewsTags(newsId)).thenReturn(expectedTags);
		List<TagTO> actualTags = tagService.getNewsTags(newsId);
		verify(tagDao, times(1)).getNewsTags(newsId);
		assertEquals(expectedTags, actualTags);
	}
}
