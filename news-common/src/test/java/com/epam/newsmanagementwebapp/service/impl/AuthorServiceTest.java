package com.epam.newsmanagementwebapp.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagementwebapp.dao.IAuthorDao;
import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.service.IAuthorService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = {"classpath:TestAppContext.xml"})
public class AuthorServiceTest {

	@Mock
	private IAuthorDao authorDao;

	@InjectMocks
	@Autowired
	private IAuthorService authorService;
	
	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}
	
	@Test
	public void testAddNewAuthor() throws Exception {
		AuthorTO author = new AuthorTO();
		author.setName("authorName");
		Long expectedId = 1L;
		when(authorDao.create(author)).thenReturn(expectedId);
		Long actualId = authorService.addNewAuthor(author);
		verify(authorDao, times(1)).create(author);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void testBindAuthorToNews() throws Exception {
		Long authorId = 1L;
		Long newsId = 1L;
		authorService.bindAuthorToNews(authorId, newsId);
		verify(authorDao, times(1)).bindAuthorToNews(authorId, newsId);
	}
	
	@Test
	public void testUnbindAuthorToNews() throws Exception {
		Long newsId = 1L;
		authorService.unbindAuthorFromNews(newsId);
		verify(authorDao, times(1)).unbindAuthorFromNews(newsId);
	}
	
	@Test
	public void testGetNewsAuthor() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "authorName");
		Long newsId = 1L;
		when(authorDao.getNewsAuthor(newsId)).thenReturn(expectedAuthor);
		AuthorTO actualAuthor = authorService.getNewsAuthor(newsId);
		verify(authorDao, times(1)).getNewsAuthor(newsId);
		assertEquals(expectedAuthor, actualAuthor);
	}
}
