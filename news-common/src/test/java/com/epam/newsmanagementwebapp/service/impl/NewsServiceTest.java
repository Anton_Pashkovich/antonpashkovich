package com.epam.newsmanagementwebapp.service.impl;

import static org.junit.Assert.assertEquals;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import com.epam.newsmanagementwebapp.dao.INewsDao;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.service.INewsService;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestAppContext.xml" })
public class NewsServiceTest {

	@Mock
	private INewsDao newsDao;

	@InjectMocks
	@Autowired
	private INewsService newsService;

	@Before
	public void setUp() {
		MockitoAnnotations.initMocks(this);
	}

	@Test
	public void testAddNews() throws Exception {
		NewsTO news = new NewsTO();
		news.setShortText("shortText");
		news.setFullText("fullText");
		news.setTitle("title");
		news.setCreationDate(Timestamp.valueOf("2007-09-23 10:10:10"));
		news.setModificationDate(Date.valueOf("2007-09-23"));
		Long expectedId = 1L;
		when(newsDao.create(news)).thenReturn(expectedId);
		Long actualId = newsService.addNews(news);
		verify(newsDao, times(1)).create(news);
		assertEquals(expectedId, actualId);
	}

	@Test
	public void testEditNews() throws Exception {
		NewsTO news = new NewsTO(1L, "shortText", "fullText", "title",
				Timestamp.valueOf("2007-09-23 10:10:10"),
				Date.valueOf("2007-09-23"));
		newsService.editNews(news);
		verify(newsDao, times(1)).update(news);
	}

	@Test
	public void testDeleteNews() throws Exception {
		Long newsId = 1L;
		newsService.deleteNews(newsId);
		verify(newsDao, times(1)).delete(newsId);
	}

	@Test
	public void testGetAllNews() throws Exception {
		List<NewsTO> expectedNews = new ArrayList<NewsTO>();
		expectedNews.add(new NewsTO(1L, "shortText1", "fullText1", "title1",
				Timestamp.valueOf("2007-09-23 10:10:10"), Date
						.valueOf("2007-09-23")));
		expectedNews.add(new NewsTO(2L, "shortText2", "fullText2", "title2",
				Timestamp.valueOf("2007-09-23 10:10:10"), Date
						.valueOf("2007-09-23")));
		when(newsDao.getAll()).thenReturn(expectedNews);
		List<NewsTO> actualNews = newsService.getAllNews();
		verify(newsDao, times(1)).getAll();
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void testGetSingleNews() throws Exception {
		NewsTO expectedNews = new NewsTO(1L, "shortText", "fullText", "title",
				Timestamp.valueOf("2007-09-23 10:10:10"), Date
				.valueOf("2007-09-23"));
		Long newsId = 1L;
		when(newsDao.read(newsId)).thenReturn(expectedNews);
		NewsTO actualNews = newsService.getSingleNews(newsId);
		verify(newsDao, times(1)).read(newsId);
		assertEquals(expectedNews, actualNews);
	}
	
	@Test
	public void testGetNewsByAuthor() throws Exception {
		Long authorId = 1L;
		List<NewsTO> expectedNews = new ArrayList<NewsTO>();
		expectedNews.add(new NewsTO(1L, "shortText1", "fullText1", "title1",
				Timestamp.valueOf("2007-09-23 10:10:10"), Date
						.valueOf("2007-09-23")));
		expectedNews.add(new NewsTO(2L, "shortText2", "fullText2", "title2",
				Timestamp.valueOf("2007-09-23 10:10:10"), Date
						.valueOf("2007-09-23")));
		when(newsDao.getNewsByAuthor(authorId)).thenReturn(expectedNews);
		List<NewsTO> actualNews = newsService.getNewsByAuthor(authorId);
		verify(newsDao, times(1)).getNewsByAuthor(authorId);
		assertEquals(expectedNews, actualNews);
	}
	
//	@Test
//	public void testGetNewsByFilter() throws Exception {
//		List<Long> tags = new ArrayList<Long>();
//		tags.add(3L);
//		List<Long> authors = new ArrayList<Long>();
//		authors.add(1L);
//		FilterState filterState = FilterState.AUTHORS_AND_TAGS;
//		Filter filter = new Filter(authors, tags, filterState);
//		List<NewsTO> expectedNews = new ArrayList<NewsTO>();
//		expectedNews.add(new NewsTO(3L, "short3", "full3", "title3", Timestamp
//				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
//		when(newsDao.getNewsByFilter(filter)).thenReturn(expectedNews);
//		List<NewsTO> actualNews = newsService.getNewsByFilter(filter);
//		verify(newsDao, times(1)).getNewsByFilter(filter);
//		assertEquals(expectedNews, actualNews);
//	}
}