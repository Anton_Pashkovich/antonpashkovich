package com.epam.newsmanagementwebapp.dao.impl;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestAppContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:authordao/AuthorTestDataSet.xml")
public class AuthorDaoTest {

	@Autowired
	private AuthorDao authorDao;

	@Test
	public void testCreate() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO();
		expectedAuthor.setName("FourthAuthor");
		Long newId = (long) authorDao.create(expectedAuthor);
		expectedAuthor.setId(newId);
		AuthorTO actualAuthor = authorDao.read(newId);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testRead() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(1L, "FirstAuthor");
		AuthorTO actualAuthor = authorDao.read(1L);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testUpdate() throws Exception {
		Long authorId = 2L;
		AuthorTO expectedAuthor = new AuthorTO(authorId, "SecondAuthorUpdate");
		authorDao.update(expectedAuthor);
		AuthorTO updatedAuthor = authorDao.read(authorId);
		assertReflectionEquals(expectedAuthor, updatedAuthor);
	}
	
	@Test
	public void testDelete() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO();
		authorDao.delete(1L);
		AuthorTO actualAuthor = authorDao.read(1L);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testGetNewsAuthor() throws Exception {
		AuthorTO expectedAuthor = new AuthorTO(2L, "SecondAuthor");
		AuthorTO actualAuthor = authorDao.getNewsAuthor(2L);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testUnbindAuthorFromNews() throws Exception {
		AuthorTO expected = new AuthorTO();
		authorDao.unbindAuthorFromNews(2L);
		AuthorTO actual = authorDao.getNewsAuthor(2L);
		assertReflectionEquals(expected, actual);
	}
	
	@Test
	public void testBindAuthorToNews() throws Exception {
		Long newsId = 3L;
		Long authorId = 3L;
		authorDao.bindAuthorToNews(authorId, newsId);
		AuthorTO expectedAuthor = new AuthorTO(authorId, "ThirdAuthor");
		AuthorTO actualAuthor = authorDao.getNewsAuthor(newsId);
		assertReflectionEquals(expectedAuthor, actualAuthor);
	}
	
	@Test
	public void testGetAll() throws Exception {
		List<AuthorTO> expectedAuthors = new ArrayList<AuthorTO>();
		expectedAuthors.add(new AuthorTO(3L, "ThirdAuthor"));
		expectedAuthors.add(new AuthorTO(1L, "FirstAuthor"));
		expectedAuthors.add(new AuthorTO(2L, "SecondAuthor"));
		List<AuthorTO> actualAuthors = authorDao.getAll();
		assertReflectionEquals(expectedAuthors, actualAuthors);
	}
}