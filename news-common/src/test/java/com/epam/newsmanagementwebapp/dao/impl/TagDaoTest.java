package com.epam.newsmanagementwebapp.dao.impl;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestAppContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:tagdao/TagTestDataSet.xml")
public class TagDaoTest {

	@Autowired
	private TagDao tagDao;

	@Test
	public void testCreate() throws Exception {
		TagTO expectedTag = new TagTO();
		expectedTag.setName("FourthAuthor");
		Long newId = tagDao.create(expectedTag);
		expectedTag.setId(newId);
		TagTO actualTag = tagDao.read(newId);
		assertReflectionEquals(expectedTag, actualTag);
	}
	
	@Test
	public void testRead() throws Exception {
		TagTO expectedTag = new TagTO(1L, "FirstTag");
		TagTO actualTag = tagDao.read(1L);
		assertReflectionEquals(expectedTag, actualTag);
	}
	
	@Test
	public void testUpdate() throws Exception {
		Long tagId = 2L;
		TagTO expectedTag = new TagTO(tagId, "SecondTagUpdate");
		tagDao.update(expectedTag);
		TagTO updatedTag = tagDao.read(tagId);
		assertReflectionEquals(expectedTag, updatedTag);
	}
	
	@Test
	public void testDelete() throws Exception {
		TagTO expected = new TagTO();
		tagDao.delete(1L);
		TagTO actual = tagDao.read(1L);
		assertReflectionEquals(expected, actual);
	}
	
	@Test
	public void testGetNewsTags() throws Exception {
		List<TagTO> expectedTags = new ArrayList<TagTO>();
		expectedTags.add(new TagTO(1L, "FirstTag"));
		expectedTags.add(new TagTO(2L, "SecondTag"));
		List<TagTO> actualTags = tagDao.getNewsTags(2L);
		assertReflectionEquals(expectedTags, actualTags);
	}
	
	@Test
	public void testUnbindAllTags() throws Exception {
		List<TagTO> expected = new ArrayList<TagTO>();
		tagDao.unbindAllTags(2L);
		List<TagTO> actual = tagDao.getNewsTags(2L);
		assertReflectionEquals(expected, actual);
	}
	
	@Test
	public void testBindTagToNews() throws Exception {
		List<TagTO> expectedTags = new ArrayList<TagTO>();
		expectedTags.add(new TagTO(2L, "SecondTag"));
		expectedTags.add(new TagTO(3L, "ThirdTag"));
		Long newsId = 3L;
		Long tagId = 2L;
		tagDao.bindTagToNews(tagId, newsId);
		List<TagTO> actualTags = tagDao.getNewsTags(newsId);
		assertReflectionEquals(expectedTags, actualTags);
	}
	
	@Test
	public void testGetAll() throws Exception {
		List<TagTO> expectedTags = new ArrayList<TagTO>();
		expectedTags.add(new TagTO(3L, "ThirdTag"));
		expectedTags.add(new TagTO(1L, "FirstTag"));
		expectedTags.add(new TagTO(2L, "SecondTag"));
		List<TagTO> actualTags = tagDao.getAll();
		assertReflectionEquals(expectedTags, actualTags);
	}
}