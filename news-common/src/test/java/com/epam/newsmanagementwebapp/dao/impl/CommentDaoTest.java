package com.epam.newsmanagementwebapp.dao.impl;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestAppContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:commentdao/CommentTestDataSet.xml")
public class CommentDaoTest {

	@Autowired
	private CommentDao commentDao;

	@Test
	public void testCreate() throws Exception {
		CommentTO expectedComment = new CommentTO();
		expectedComment.setNewsId(1L);
		expectedComment.setCommentText("text4");
		expectedComment.setCreationDate(Timestamp
				.valueOf("2007-09-23 10:10:10"));
		Long newId = commentDao.create(expectedComment);
		expectedComment.setId(newId);
		CommentTO actualComment = commentDao.read(newId);
		assertReflectionEquals(expectedComment, actualComment);
	}

	@Test
	public void testRead() throws Exception {
		CommentTO expectedComment = new CommentTO(1L, 2L, "text1",
				Timestamp.valueOf("2007-09-23 10:10:10"));
		CommentTO actualcomment = commentDao.read(1L);
		assertReflectionEquals(expectedComment, actualcomment);
	}

	@Test
	public void testUpdate() throws Exception {
		CommentTO expectedNews = new CommentTO(1L, 2L, "text1",
				Timestamp.valueOf("2007-09-23 10:10:10"));
		commentDao.update(expectedNews);
		CommentTO updatedNews = commentDao.read(1L);
		assertReflectionEquals(expectedNews, updatedNews);
	}

	@Test
	public void testDelete() throws Exception {
		CommentTO expected = new CommentTO();
		commentDao.delete(2L);
		CommentTO actual = commentDao.read(2L);
		assertReflectionEquals(expected, actual);
	}

	@Test
	public void getNewsComments() throws Exception {
		List<CommentTO> expectedComments = new ArrayList<CommentTO>();
		expectedComments.add(new CommentTO(1L, 2L, "text1", Timestamp
				.valueOf("2007-09-23 10:10:10")));
		expectedComments.add(new CommentTO(2L, 2L, "text2", Timestamp
				.valueOf("2007-09-23 10:10:10")));
		List<CommentTO> actualComments = commentDao.getNewsComments(2L);
		assertReflectionEquals(expectedComments, actualComments);
	}
}