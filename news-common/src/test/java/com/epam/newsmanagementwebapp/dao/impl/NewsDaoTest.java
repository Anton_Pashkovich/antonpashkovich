package com.epam.newsmanagementwebapp.dao.impl;

import static org.unitils.reflectionassert.ReflectionAssert.assertReflectionEquals;

import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.test.context.support.DependencyInjectionTestExecutionListener;

import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.github.springtestdbunit.DbUnitTestExecutionListener;
import com.github.springtestdbunit.TransactionDbUnitTestExecutionListener;
import com.github.springtestdbunit.annotation.DatabaseSetup;

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = { "classpath:TestAppContext.xml" })
@TestExecutionListeners({ DependencyInjectionTestExecutionListener.class,
		DbUnitTestExecutionListener.class,
		TransactionDbUnitTestExecutionListener.class })
@DatabaseSetup("classpath:newsdao/NewsTestDataSet.xml")
public class NewsDaoTest {

	@Autowired
	private NewsDao newsDao;

	@Test
	public void testCreate() throws Exception {
		NewsTO expectedNews = new NewsTO();
		expectedNews.setShortText("shortText4");
		expectedNews.setFullText("fullText4");
		expectedNews.setTitle("title4");
		expectedNews.setCreationDate(Timestamp.valueOf("2007-09-23 10:10:10"));
		expectedNews.setModificationDate(Date.valueOf("2007-09-23"));
		Long newId = newsDao.create(expectedNews);
		expectedNews.setId(newId);
		NewsTO actualNews = newsDao.read(newId);
		assertReflectionEquals(expectedNews, actualNews);
	}

	@Test
	public void testRead() throws Exception {
		NewsTO expectedNews = new NewsTO(1L, "short1", "full1", "title1",
				Timestamp.valueOf("2007-09-23 10:10:10"),
				Date.valueOf("2007-09-23"));
		NewsTO actualNews = newsDao.read(1L);
		assertReflectionEquals(expectedNews, actualNews);
	}

	@Test
	public void testUpdate() throws Exception {
		Long newsId = 2L;
		NewsTO expectedNews = new NewsTO(newsId, "short2update", "full2update",
				"title2update", Timestamp.valueOf("2007-09-23 10:10:10"),
				Date.valueOf("2007-09-23"));
		newsDao.update(expectedNews);
		NewsTO updatedNews = newsDao.read(newsId);
		assertReflectionEquals(expectedNews, updatedNews);
	}

	@Test
	public void testDelete() throws Exception {
		NewsTO expected = new NewsTO();
		newsDao.delete(2L);
		NewsTO actual = newsDao.read(2L);
		assertReflectionEquals(expected, actual);
	}

	@Test
	public void testGetAll() throws Exception {
		List<NewsTO> expectedNews = new ArrayList<NewsTO>();
		expectedNews.add(new NewsTO(2L, "short2", "full2", "title2", Timestamp
				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
		expectedNews.add(new NewsTO(3L, "short3", "full3", "title3", Timestamp
				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
		expectedNews.add(new NewsTO(1L, "short1", "full1", "title1", Timestamp
				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
		List<NewsTO> actualNews = newsDao.getAll();
		assertReflectionEquals(expectedNews, actualNews);
	}
	
	@Test
	public void testGetNewsByAuthor() throws Exception {
		List<NewsTO> expectedNews = new ArrayList<NewsTO>();
		expectedNews.add(new NewsTO(3L, "short3", "full3", "title3", Timestamp
				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
		expectedNews.add(new NewsTO(1L, "short1", "full1", "title1", Timestamp
				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
		List<NewsTO> actualNews = newsDao.getNewsByAuthor(1L);
		assertReflectionEquals(expectedNews, actualNews);
	}
	
//	@Test
//	public void testGetNewsByFilter() throws Exception {
//		List<Long> tags = new ArrayList<Long>();
//		tags.add(3L);
//		List<Long> authors = new ArrayList<Long>();
//		authors.add(1L);
//		FilterState filterState = FilterState.AUTHORS_AND_TAGS;
//		Filter filter = new Filter(authors, tags, filterState);
//		List<NewsTO> expectedNews = new ArrayList<NewsTO>();
//		expectedNews.add(new NewsTO(3L, "short3", "full3", "title3", Timestamp
//				.valueOf("2007-09-23 10:10:10"), Date.valueOf("2007-09-23")));
//		List<NewsTO> actualNews = newsDao.getNewsByFilter(filter);
//		assertReflectionEquals(expectedNews, actualNews);
//	}
}