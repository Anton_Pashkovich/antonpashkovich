package com.epam.newsmanagementwebapp.exception;

public final class DaoException extends Exception {

	private static final long serialVersionUID = 7734295897336444577L;

	public DaoException() {
	}

	public DaoException(String message) {
		super(message);
	}

	public DaoException(String message, Throwable cause) {
		super(message, cause);
	}

	public DaoException(Throwable cause) {
		super(cause);
	}
}
