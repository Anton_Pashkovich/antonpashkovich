package com.epam.newsmanagementwebapp.dao;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.exception.DaoException;

/**
 * Interface contains CommentDao methods and extends generic interface with CRUD
 * operations
 */
public interface ICommentDao extends IGenericDao<CommentTO>{

	/**
	 * Returns list of comments from appropriate news
	 */
	List<CommentTO> getNewsComments(Long newsId) throws DaoException;
}