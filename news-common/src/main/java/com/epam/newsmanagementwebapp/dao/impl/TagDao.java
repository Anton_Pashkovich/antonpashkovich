package com.epam.newsmanagementwebapp.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagementwebapp.dao.ITagDao;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.exception.DaoException;

public class TagDao implements ITagDao {

	private static final String DELETE_QUERY = "DELETE FROM TAG WHERE TAG_ID= ?";
	private static final String UPDATE_QUERY = "UPDATE TAG SET TAG_NAME  = ? "
			+ "WHERE TAG_ID = ?";
//	private static final String CREATE_QUERY = "INSERT INTO TAG (TAG_ID, TAG_NAME) "
//			+ "VALUES (TAG_ID_SEQ.nextval, ?)";
	private static final String SELECT_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG ORDER BY lower(TAG_NAME)";
	private static final String SELECT_BY_PK_QUERY = "SELECT TAG_ID, TAG_NAME FROM TAG "
			+ "WHERE TAG_ID = ?";
	private static final String BIND_TO_NEWS_QUERY = "INSERT INTO NEWS_TAG "
			+ "(NEWS_ID, TAG_ID) VALUES (?,?)";
	private static final String UNBIND_ALL_TAGS_QUERY = "DELETE FROM NEWS_TAG "
			+ "WHERE NEWS_ID= ?";
	private static final String GET_NEWS_TAGS_QUERY = "SELECT TAG.TAG_ID, TAG.TAG_NAME "
			+ "FROM TAG JOIN NEWS_TAG ON TAG.TAG_ID = NEWS_TAG.TAG_ID "
			+ "WHERE NEWS_TAG.NEWS_ID = ?";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public Long create(TagTO object) {
		entityManager.persist(object);
		Long id = object.getId();
		return id;
	}

	@Override
	@Transactional
	public TagTO read(Long key) {
		Query query = entityManager.createNativeQuery(SELECT_BY_PK_QUERY, TagTO.class);
		query.setParameter(1, key);
		return (TagTO) query.getSingleResult();
	}

	@Override
	@Transactional
	public void update(TagTO object) {
		Query query = entityManager.createNativeQuery(UPDATE_QUERY);
		query.setParameter(1, object.getName());
		query.setParameter(2, object.getId());
		query.executeUpdate();
		entityManager.flush();
	}

	@Override
	@Transactional
	public void delete(Long objectId) {
		Query query = entityManager.createNativeQuery(DELETE_QUERY);
		query.setParameter(1, objectId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<TagTO> getAll() {
		Query query = entityManager.createNativeQuery(SELECT_QUERY, TagTO.class);
		return (List<TagTO>)query.getResultList();
	}

	@Override
	@Transactional
	public void bindTagToNews(Long tagId, Long newsId) {
		Query query = entityManager.createNativeQuery(BIND_TO_NEWS_QUERY);
		query.setParameter(1, newsId);
		query.setParameter(2, tagId);
		query.executeUpdate();
	}

	@Override
	public void bindTagsToNews(List<Long> tags, Long newsId) {
		for (Long tagId: tags) {
			bindTagToNews(tagId, newsId);
		}
		entityManager.flush();
	}

	@Override
	@Transactional
	public void unbindAllTags(Long newsId) {
		Query query = entityManager.createNativeQuery(UNBIND_ALL_TAGS_QUERY);
		query.setParameter(1, newsId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<TagTO> getNewsTags(Long newsId) {
		Query query = entityManager.createNativeQuery(GET_NEWS_TAGS_QUERY, TagTO.class);
		query.setParameter(1, newsId);
		return (List<TagTO>)query.getResultList();
	}

	@Override
	public void delete(List<Long> newsIdList) {
	}

}