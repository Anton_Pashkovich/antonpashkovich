package com.epam.newsmanagementwebapp.dao;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.exception.DaoException;
import com.epam.newsmanagementwebapp.util.Filter;

/**
 * Interface contains NewsDao methods and extends generic interface with CRUD
 * operations
 */
public interface INewsDao extends IGenericDao<NewsTO> {

	/**
	 * Returns a list of News by concrete author
	 * 
	 * @throws DaoException
	 */
	List<NewsTO> getNewsByAuthor(Long authorId) throws DaoException;

	Long getPreviousId(Filter filter, Long newsId) throws DaoException;

	Long getNextId(Filter filter, Long newsId) throws DaoException;

	Long getPagesCount(Filter filter) throws DaoException;

	List<NewsTO> getNewsByFilter(Filter filter, Long pageNum)
			throws DaoException;
}