package com.epam.newsmanagementwebapp.dao.impl;

import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagementwebapp.dao.ICommentDao;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.exception.DaoException;

public class CommentDao implements ICommentDao {

	private static final String DELETE_QUERY = "DELETE FROM COMMENTS WHERE COMMENT_ID= ?";
	private static final String UPDATE_QUERY = "UPDATE COMMENTS "
			+ "SET COMMENT_TEXT = ?, CREATION_DATE = ?, NEWS_ID = ? WHERE COMMENT_ID = ?";
//	private static final String CREATE_QUERY = "INSERT INTO COMMENTS "
//			+ "(COMMENT_ID, COMMENT_TEXT, CREATION_DATE, NEWS_ID) "
//			+ "VALUES (COMMENT_ID_SEQ.nextval, ?, ?, ?)";
	private static final String SELECT_QUERY = "SELECT COMMENT_ID, COMMENT_TEXT, "
			+ "CREATION_DATE, NEWS_ID FROM COMMENTS";
	private static final String SELECT_BY_PK_QUERY = "SELECT COMMENT_ID, COMMENT_TEXT, "
			+ "CREATION_DATE, NEWS_ID FROM COMMENTS WHERE COMMENT_ID = ?";
	private static final String SELECT_BY_NEWS_ID = "SELECT COMMENT_ID, COMMENT_TEXT, "
			+ "CREATION_DATE, NEWS_ID FROM COMMENTS WHERE NEWS_ID = ? ORDER BY CREATION_DATE";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public Long create(CommentTO object) {
		entityManager.persist(object);
		Long id = object.getId();
		return id;
	}

	@Override
	@Transactional
	public CommentTO read(Long key) {
		Query query = entityManager.createNativeQuery(SELECT_BY_PK_QUERY, CommentTO.class);
		query.setParameter(1, key);
		return (CommentTO) query.getSingleResult();
	}

	@Override
	@Transactional
	public void update(CommentTO object) {
		Query query = entityManager.createNativeQuery(UPDATE_QUERY);
		query.setParameter(1, object.getCommentText());
		query.setParameter(2, object.getCreationDate());
		query.setParameter(3, object.getNewsId());
		query.setParameter(4, object.getId());
		query.executeUpdate();
	}

	@Override
	@Transactional
	public void delete(Long objectId) {
		Query query = entityManager.createNativeQuery(DELETE_QUERY);
		query.setParameter(1, objectId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CommentTO> getAll() {
		Query query = entityManager.createNativeQuery(SELECT_QUERY, CommentTO.class);
		return (List<CommentTO>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<CommentTO> getNewsComments(Long newsId) {
		Query query = entityManager.createNativeQuery(SELECT_BY_NEWS_ID, CommentTO.class);
		query.setParameter(1, newsId);
		return (List<CommentTO>)query.getResultList();
	}

	@Override
	public void delete(List<Long> newsIdList) {		
	}
}