package com.epam.newsmanagementwebapp.dao.impl;

import java.util.Date;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagementwebapp.dao.IAuthorDao;
import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.exception.DaoException;

public class AuthorDao implements IAuthorDao {

	private static final String DELETE_QUERY = "DELETE FROM AUTHOR WHERE AUTHOR_ID= ?";
	private static final String UPDATE_QUERY = "UPDATE AUTHOR SET NAME  = ? "
			+ "WHERE AUTHOR_ID = ?";
//	private static final String CREATE_QUERY = "INSERT INTO AUTHOR (AUTHOR_ID, NAME) "
//			+ "VALUES (AUTHOR_ID_SEQ.nextval, ?)";
	private static final String EXPIRE_QUERY = "UPDATE AUTHOR SET EXPIRED = ? WHERE AUTHOR_ID = ?";
	private static final String GET_ACTUAL_AUTHORS_QUERY = "SELECT AUTHOR_ID, NAME FROM AUTHOR WHERE EXPIRED IS NULL ORDER BY lower(NAME)";
	private static final String SELECT_QUERY = "SELECT AUTHOR_ID, NAME FROM AUTHOR ORDER BY lower(NAME)";
	private static final String SELECT_BY_PK_QUERY = "SELECT AUTHOR_ID, NAME "
			+ "FROM AUTHOR WHERE AUTHOR_ID = ?";
	private static final String BIND_TO_NEWS_QUERY = "INSERT INTO NEWS_AUTHOR "
			+ "(NEWS_ID, AUTHOR_ID) VALUES (?,?)";
	private static final String UNBIND_FROM_NEWS_QUERY = "DELETE FROM NEWS_AUTHOR "
			+ "WHERE NEWS_ID= ?";
	private static final String GET_NEWS_AUTHOR_QUERY = "SELECT AUTHOR.AUTHOR_ID, AUTHOR.NAME "
			+ "FROM AUTHOR JOIN NEWS_AUTHOR ON AUTHOR.AUTHOR_ID = NEWS_AUTHOR.AUTHOR_ID "
			+ "WHERE NEWS_AUTHOR.NEWS_ID = ?";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public Long create(AuthorTO object) {
		entityManager.persist(object);
		Long id = object.getId();
		return id;
	}

	@Override
	@Transactional
	public AuthorTO read(Long key) {
		Query query = entityManager.createNativeQuery(SELECT_BY_PK_QUERY, AuthorTO.class);
		query.setParameter(1, key);
		return (AuthorTO) query.getSingleResult();
	}

	@Override
	@Transactional
	public void update(AuthorTO object) {
		Query query = entityManager.createNativeQuery(UPDATE_QUERY);
		query.setParameter(1, object.getName());
		query.setParameter(2, object.getId());
		query.executeUpdate();
		entityManager.flush();
	}

	@Override
	@Transactional
	public void delete(Long objectId) {
		Query query = entityManager.createNativeQuery(DELETE_QUERY);
		query.setParameter(1, objectId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<AuthorTO> getAll() {
		Query query = entityManager.createNativeQuery(SELECT_QUERY, AuthorTO.class);
		return (List<AuthorTO>)query.getResultList();
	}

	@Override
	@Transactional
	public void bindAuthorToNews(Long authorId, Long newsId)
			throws DaoException {
		Query query = entityManager.createNativeQuery(BIND_TO_NEWS_QUERY);
		query.setParameter(1, newsId);
		query.setParameter(2, authorId);
		query.executeUpdate();
		entityManager.flush();
	}

	@Override
	@Transactional
	public void unbindAuthorFromNews(Long newsId) {
		Query query = entityManager.createNativeQuery(UNBIND_FROM_NEWS_QUERY);
		query.setParameter(1, newsId);
		query.executeUpdate();
		entityManager.flush();
	}

	@Override
	@Transactional
	public AuthorTO getNewsAuthor(Long newsId) {
		Query query = entityManager.createNativeQuery(GET_NEWS_AUTHOR_QUERY, AuthorTO.class);
		query.setParameter(1, newsId);
		return (AuthorTO) query.getSingleResult();
	}

	@Override
	public void delete(List<Long> newsIdList) {
	}

	@Override
	@Transactional
	public void expireAuthor(Long authorId) {
		Query query = entityManager.createNativeQuery(EXPIRE_QUERY);
		query.setParameter(1, new Date());
		query.setParameter(2, authorId);
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<AuthorTO> getActualAuthors() {
		Query query = entityManager.createNativeQuery(GET_ACTUAL_AUTHORS_QUERY, AuthorTO.class);
		return (List<AuthorTO>)query.getResultList();
	}
}