package com.epam.newsmanagementwebapp.dao;

import java.util.List;

import com.epam.newsmanagementwebapp.exception.DaoException;

/**
 * Unified interface with CRUD operations
 * 
 * @param <T>
 *            object type
 */
public interface IGenericDao<T> {

	/**
	 * Creates a new record
	 */
	Long create(T object) throws DaoException;
	
	/**
	 * Returns an object with appropriate primary key or null
	 */
	T read(Long key) throws DaoException;

	/**
	 * Updates an object in database
	 */
	void update(T object) throws DaoException;

	/**
	 * Deletes an object from database
	 */
	void delete(Long objectId) throws DaoException;

	/**
	 * Returns a list of all objects from database
	 */
	List<T> getAll() throws DaoException;

	void delete(List<Long> newsIdList) throws DaoException;
}