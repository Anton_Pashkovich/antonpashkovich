package com.epam.newsmanagementwebapp.dao;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.exception.DaoException;

/**
 * Interface contains AuthorDao methods and extends generic interface with CRUD
 * operations
 */
public interface IAuthorDao extends IGenericDao<AuthorTO> {

	/**
	 * This method binds author with some news
	 * 
	 * @throws DaoException
	 */
	void bindAuthorToNews(Long authorId, Long newsId) throws DaoException;
	
	/**
	 * This method unbinds author from current news
	 * 
	 * @throws DaoException
	 */
	void unbindAuthorFromNews(Long newsId) throws DaoException;

	/**
	 * This method gets news author by appropriate news id
	 * 
	 * @throws DaoException
	 */
	AuthorTO getNewsAuthor(Long newsId) throws DaoException;

	void expireAuthor(Long authorId) throws DaoException;

	List<AuthorTO> getActualAuthors() throws DaoException;
}
