package com.epam.newsmanagementwebapp.dao;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.exception.DaoException;

/**
 * Interface contains TagDao methods and extends generic interface with CRUD
 * operations
 */
public interface ITagDao extends IGenericDao<TagTO> {

	/**
	 * This method binds tag with some news
	 */
	void bindTagToNews(Long tagId, Long newsId) throws DaoException;

	/**
	 * This method unbinds all tags from current news
	 */
	void unbindAllTags(Long newsId) throws DaoException;

	/**
	 * This method returns all tags binded to appropriate news
	 */
	List<TagTO> getNewsTags(Long newsId) throws DaoException;

	void bindTagsToNews(List<Long> tags, Long newsId) throws DaoException;
}