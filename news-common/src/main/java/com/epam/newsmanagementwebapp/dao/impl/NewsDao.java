package com.epam.newsmanagementwebapp.dao.impl;

import java.math.BigDecimal;
import java.util.Iterator;
import java.util.List;

import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagementwebapp.dao.INewsDao;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.exception.DaoException;
import com.epam.newsmanagementwebapp.util.Filter;

public class NewsDao implements INewsDao {

	private static final int NEWS_ON_PAGE_CNT = 3;
	private static final String ORDER_BY = " ORDER BY C.CNT DESC NULLS LAST";
	private static final String TAGS_JOIN_PART2 = ") ) nt ON nt.news_id = n.news_id";
	private static final String TAGS_JOIN_PART1 = " INNER JOIN (SELECT DISTINCT news_id FROM News_Tag WHERE News_Tag.tag_id IN(";
	private static final String AUTHOR_JOIN_PART2 = ") na ON na.news_id = n.news_id ";
	private static final String AUTHOR_JOIN_PART1 = " INNER JOIN (SELECT news_id, author_id FROM News_Author WHERE author_id = ";
	private static final String SELECT_FROM_NUMBERED_QUERY1 = ") q) select q.NEWS_ID from numbered_query q, (select min(idx) as idx from numbered_query where news_id=";
	private static final String SELECT_FROM_NUMBERED_QUERY2 = ") ni where q.idx in (";
	private static final String WITH_NUMBERED_QUERY_AS = "with numbered_query as (select q.*, rownum as idx from (";
	private static final String SELECT_WITH_ROWNUM = "select NEWS_ID, SHORT_TEXT, FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE, VERSION from (select q.*, rownum as idx from (";
	private static final String DELETE_QUERY = "DELETE FROM NEWS WHERE NEWS_ID IN (";
//	private static final String UPDATE_QUERY = "UPDATE NEWS SET SHORT_TEXT = ?, "
//			+ "FULL_TEXT = ?, TITLE = ?, MODIFICATION_DATE = ? "
//			+ "WHERE NEWS_ID = ?";
	private static final String SELECT_QUERY = "SELECT n.NEWS_ID, n.SHORT_TEXT, "
			+ "n.FULL_TEXT, n.TITLE, n.CREATION_DATE, "
			+ "n.MODIFICATION_DATE, n.VERSION, c.CNT FROM NEWS n "
			+ "LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM COMMENTS GROUP BY NEWS_ID) c "
			+ "ON n.NEWS_ID = c.NEWS_ID ";
	private static final String SELECT_NEWS_ID_LIST_QUERY = "SELECT n.NEWS_ID, c.CNT FROM NEWS n "
			+ "LEFT JOIN (SELECT NEWS_ID, COUNT(*) CNT FROM COMMENTS GROUP BY NEWS_ID) c "
			+ "ON n.NEWS_ID = c.NEWS_ID ";
	private static final String SELECT_BY_PK_QUERY = "SELECT NEWS_ID, SHORT_TEXT, "
			+ "FULL_TEXT, TITLE, CREATION_DATE, MODIFICATION_DATE, VERSION "
			+ "FROM NEWS WHERE NEWS_ID = ?";
	private static final String SELECT_BY_AUTHOR_QUERY = "SELECT NEWS.NEWS_ID, "
			+ "NEWS.SHORT_TEXT, NEWS.FULL_TEXT, NEWS.TITLE, NEWS.CREATION_DATE, "
			+ "NEWS.MODIFICATION_DATE FROM NEWS JOIN NEWS_AUTHOR "
			+ "ON NEWS.NEWS_ID = NEWS_AUTHOR.NEWS_ID WHERE NEWS_AUTHOR.AUTHOR_ID = ?";

	@PersistenceContext
	private EntityManager entityManager;

	@Override
	@Transactional
	public Long create(NewsTO object) {
		entityManager.persist(object);
		entityManager.flush();
		Long id = object.getId();
		return id;
	}

	@Override
	@Transactional
	public NewsTO read(Long key) {
		Query query = entityManager.createNativeQuery(SELECT_BY_PK_QUERY, NewsTO.class);
		query.setParameter(1, key);
		entityManager.flush();
		return (NewsTO) query.getSingleResult();
	}

	@Override
	@Transactional
	public void update(NewsTO object) {
		entityManager.merge(object);
	}

	@Override
	@Transactional
	public void delete(List<Long> newsIdList) {
		Query query = entityManager.createNativeQuery(buildDeleteQuery(newsIdList));
		query.executeUpdate();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getAll() {
		Query query = entityManager.createNativeQuery(SELECT_QUERY, NewsTO.class);
		return (List<NewsTO>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getNewsByAuthor(Long authorId) {
		Query query = entityManager.createNativeQuery(SELECT_BY_AUTHOR_QUERY, NewsTO.class);
		query.setParameter(1, authorId);
		return (List<NewsTO>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public List<NewsTO> getNewsByFilter(Filter filter, Long pageNum) {
		Query query = entityManager.createNativeQuery(getSQLByFilter(filter, pageNum), NewsTO.class);
		return (List<NewsTO>)query.getResultList();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Long getNextId(Filter filter, Long newsId) {
		Query query = entityManager.createNativeQuery(buildNextNewsQuery(filter, newsId, "ni.idx+1"));
		List<BigDecimal> list = (List<BigDecimal>)query.getResultList();
		if (list.isEmpty()) {
			return null;
		}
		return list.iterator().next().longValue();
	}

	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Long getPreviousId(Filter filter, Long newsId) {
		Query query = entityManager.createNativeQuery(buildNextNewsQuery(filter, newsId, "ni.idx-1"));
		List<BigDecimal> list = (List<BigDecimal>)query.getResultList();
		if (list.isEmpty()) {
			return null;
		}
		return list.iterator().next().longValue();
	}
	
	@SuppressWarnings("unchecked")
	@Override
	@Transactional
	public Long getPagesCount(Filter filter) {
		Query query = entityManager.createNativeQuery(getNewsIdListQuery(filter));
		List<Long> newsIdList = (List<Long>)query.getResultList();
		Long cnt = (long) (newsIdList.size() / NEWS_ON_PAGE_CNT);
		Long mod = (long) (newsIdList.size() % NEWS_ON_PAGE_CNT);
		if (mod > 0) {
			return cnt + 1;
		}
		return cnt;
	}
	
	@Override
	public void delete(Long objectId) {
	}


	private String buildNextNewsQuery(Filter filter, long newsId, String direction) {
		StringBuilder query = new StringBuilder();
		query.append(WITH_NUMBERED_QUERY_AS);
		query.append(getNewsIdListQuery(filter));
		query.append(SELECT_FROM_NUMBERED_QUERY1);
		query.append(newsId);
		query.append(SELECT_FROM_NUMBERED_QUERY2);
		query.append(direction);
		query.append(")");
		return query.toString();
	}
	
	private String buildDeleteQuery(List<Long> newsIdList) {
		StringBuilder query = new StringBuilder();
		query.append(DELETE_QUERY);
		for (Long newsId: newsIdList) {
			query.append(newsId);
			query.append(",");
		}
		query.deleteCharAt(query.length()-1);
		query.append(")");
		return query.toString();
	}

	private String getSQLByFilter(Filter filter, Long pageNum) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(SELECT_WITH_ROWNUM);
		sqlQuery.append(SELECT_QUERY);
		if (filter != null && filter.getAuthor() != null) {
			sqlQuery.append(AUTHOR_JOIN_PART1).append(filter.getAuthor())
					.append(AUTHOR_JOIN_PART2);
		}

		if (filter != null && filter.getTags() != null && !filter.getTags().isEmpty()) {
			sqlQuery.append(TAGS_JOIN_PART1);
			Iterator<Long> iter = filter.getTags().iterator();
			while (iter.hasNext()) {
				Long tagId = iter.next();
				sqlQuery.append(tagId);
				if (iter.hasNext()) {
					sqlQuery.append(",");
				} else {
					sqlQuery.append(TAGS_JOIN_PART2);
				}
			}
		}
		sqlQuery.append(ORDER_BY);
		sqlQuery.append(") q) where idx between ");
		sqlQuery.append((pageNum - 1) * Filter.newsOnPageCnt + 1);
		sqlQuery.append(" and ");
		sqlQuery.append(pageNum * Filter.newsOnPageCnt);
		return sqlQuery.toString();
	}
	
	private String getNewsIdListQuery(Filter filter) {
		StringBuilder sqlQuery = new StringBuilder();
		sqlQuery.append(SELECT_NEWS_ID_LIST_QUERY);
		if (filter == null) {
			sqlQuery.append(ORDER_BY);
			return sqlQuery.toString();
		}
		if (filter.getAuthor() != null) {
			sqlQuery.append(AUTHOR_JOIN_PART1).append(filter.getAuthor())
					.append(AUTHOR_JOIN_PART2);
		}
		if (filter.getTags() != null && !filter.getTags().isEmpty()) {
			sqlQuery.append(TAGS_JOIN_PART1);
			Iterator<Long> iter = filter.getTags().iterator();
			while (iter.hasNext()) {
				Long tagId = iter.next();
				sqlQuery.append(tagId);
				if (iter.hasNext()) {
					sqlQuery.append(",");
				} else {
					sqlQuery.append(TAGS_JOIN_PART2);
				}
			}
		}
		sqlQuery.append(ORDER_BY);
		return sqlQuery.toString();
	}
}