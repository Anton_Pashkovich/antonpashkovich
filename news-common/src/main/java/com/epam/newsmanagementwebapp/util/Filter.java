package com.epam.newsmanagementwebapp.util;

import java.util.List;

public class Filter {
	public static final int newsOnPageCnt = 3;
	private Long author;
	private List<Long> tags;

	public Filter() {
	}

	public Filter(Long author, List<Long> tags) {
		setAuthor(author);
		setTags(tags);
	}

	public Long getAuthor() {
		return author;
	}

	public void setAuthor(Long author) {
		if (author != null) {
			this.author = author;
		}
	}

	public List<Long> getTags() {
		return tags;
	}

	public void setTags(List<Long> tags) {
		if (tags != null) {
			this.tags = tags;
		}
	}
}