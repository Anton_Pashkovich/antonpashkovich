package com.epam.newsmanagementwebapp.util;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;

import javax.sql.DataSource;

import org.springframework.jdbc.datasource.DataSourceUtils;

import com.epam.newsmanagementwebapp.exception.DaoException;

public class DBUtils {
	public static void closeResources(ResultSet rs, Connection connection,
			PreparedStatement statement, DataSource dataSource)
			throws DaoException {
		
		try {
			if (rs != null) {
				rs.close();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		}
		
		closeResources(connection, statement, dataSource);
	}

	public static void closeResources(Connection connection,
			PreparedStatement statement, DataSource dataSource)
			throws DaoException {
		if (connection != null) {
			DataSourceUtils.releaseConnection(connection, dataSource);
		}
		try {
			if (statement != null) {
				statement.close();
			}
		} catch (SQLException e) {
			throw new DaoException(e);
		}
	}
}
