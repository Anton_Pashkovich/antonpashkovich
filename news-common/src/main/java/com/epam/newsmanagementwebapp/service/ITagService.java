package com.epam.newsmanagementwebapp.service;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.exception.ServiceException;

/**
 * Interface ITagService contains TagService operations
 */
public interface ITagService {
	/**
	 * Method addNewTag creates a new tag record
	 * 
	 * @param tag
	 *            is a new tag
	 * @return created tag's id
	 * @throws ServiceException
	 */
	Long addNewTag(TagTO tag) throws ServiceException;

	/**
	 * Method bindTagToNews binds concrete tag with some news
	 * 
	 * @param tagId
	 *            is apprioriate tag id
	 * @param newsId
	 *            is apprioriate news id
	 * @throws ServiceException
	 */
	void bindTagToNews(Long tagId, Long newsId) throws ServiceException;

	/**
	 * Method unbindAllTags unbinds all tags from concrete news
	 * 
	 * @param newsId
	 *            is appropriate news id
	 * @throws ServiceException
	 */
	void unbindAllTags(Long newsId) throws ServiceException;

	/**
	 * Method getNewsTags returns appropriate news tag list
	 * 
	 * @param newsId
	 *            is required news id
	 * @return tag list
	 * @throws ServiceException
	 */
	List<TagTO> getNewsTags(Long newsId) throws ServiceException;

	List<TagTO> getAll() throws ServiceException;

	void bindTagsToNews(List<Long> tags, Long newsId) throws ServiceException;

	void deleteTag(Long tagId) throws ServiceException;

	void updateTag(TagTO tag) throws ServiceException;
}