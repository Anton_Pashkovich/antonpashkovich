package com.epam.newsmanagementwebapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagementwebapp.dao.INewsDao;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.exception.DaoException;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.INewsService;
import com.epam.newsmanagementwebapp.util.Filter;

public class NewsService implements INewsService {

	@Autowired
	private INewsDao newsDao;
	
	public Long addNews(NewsTO news) throws ServiceException {
		try {
			return newsDao.create(news);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	public void editNews(NewsTO news) throws ServiceException {
		try {
			newsDao.update(news);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	public void deleteNews(List<Long> newsIdList) throws ServiceException {
		try {
			newsDao.delete(newsIdList);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<NewsTO> getAllNews() throws ServiceException {
		try {
			return newsDao.getAll();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public NewsTO getSingleNews(Long newsId) throws ServiceException {
		try {
			return newsDao.read(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException {
		try {
			return newsDao.getNewsByAuthor(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<NewsTO> getNewsByFilter(Filter filter, Long pageNum) throws ServiceException {
		try {
			return newsDao.getNewsByFilter(filter, pageNum);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public Long getNextId(Filter filter, Long newsId) throws ServiceException {
		try {
			return newsDao.getNextId(filter, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public Long getPreviousId(Filter filter, Long newsId) throws ServiceException {
		try {
			return newsDao.getPreviousId(filter, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public Long getPagesCount(Filter filter) throws ServiceException {
		try {
			return newsDao.getPagesCount(filter);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteNews(Long newsId) throws ServiceException {
	}
}