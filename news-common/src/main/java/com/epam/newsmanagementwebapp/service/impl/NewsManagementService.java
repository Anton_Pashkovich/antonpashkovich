package com.epam.newsmanagementwebapp.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.transaction.annotation.Transactional;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.entity.valueobject.NewsVO;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.IAuthorService;
import com.epam.newsmanagementwebapp.service.ICommentService;
import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.service.INewsService;
import com.epam.newsmanagementwebapp.service.ITagService;
import com.epam.newsmanagementwebapp.util.Filter;

public class NewsManagementService implements INewsManagementService {

	@Autowired
	private IAuthorService authorService;

	@Autowired
	private ITagService tagService;

	@Autowired
	private ICommentService commentService;

	@Autowired
	private INewsService newsService;

	@Override
	@Transactional
	public Long addNews(NewsTO news, Long authorId, List<Long> tags)
			throws ServiceException {
		Long newsId = newsService.addNews(news);
		authorService.bindAuthorToNews(authorId, newsId);
		if (tags != null) {
			tagService.bindTagsToNews(tags, newsId);
		}
		return newsId;
	}

	@Override
	public void deleteNews(List<Long> newsIdList) throws ServiceException {
		newsService.deleteNews(newsIdList);
	}

	@Override
	@Transactional
	public void editNews(NewsTO news, Long authorId, List<Long> tags)
			throws ServiceException {
		newsService.editNews(news);
		authorService.unbindAuthorFromNews(news.getId());
		authorService.bindAuthorToNews(authorId, news.getId());
		tagService.unbindAllTags(news.getId());
		if (tags != null) {
			tagService.bindTagsToNews(tags, news.getId());
		}
	}

	@Override
	public List<NewsVO> getAllNews() throws ServiceException {
		List<NewsTO> newsTOlist = new ArrayList<NewsTO>();
		newsTOlist = newsService.getAllNews();
		return fillNewsVOList(newsTOlist);
	}

	@Override
	public NewsVO getSingleNews(Long newsId) throws ServiceException {
		NewsVO newsVO = new NewsVO();
		newsVO.setNews(newsService.getSingleNews(newsId));
		newsVO.setAuthor(authorService.getNewsAuthor(newsId));
		newsVO.setTags(tagService.getNewsTags(newsId));
		newsVO.setComments(commentService.getNewsComments(newsId));
		return newsVO;
	}

	@Override
	public Long addNewsAuthor(AuthorTO author) throws ServiceException {
		return authorService.addNewAuthor(author);
	}

	@Override
	public List<NewsVO> getNewsByAuthor(Long authorId) throws ServiceException {
		List<NewsTO> newsTOlist = new ArrayList<NewsTO>();
		newsTOlist = newsService.getNewsByAuthor(authorId);
		return fillNewsVOList(newsTOlist);
	}

	@Override
	public List<NewsVO> getNewsByFilter(Filter filter, Long pageNum)
			throws ServiceException {
		List<NewsTO> newsTOlist = new ArrayList<NewsTO>();
		newsTOlist = newsService.getNewsByFilter(filter, pageNum);
		return fillNewsVOList(newsTOlist);
	}

	@Override
	public Long getNextId(Filter filter, Long newsId) throws ServiceException {
		return newsService.getNextId(filter, newsId);
	}

	@Override
	public Long getPreviousId(Filter filter, Long newsId)
			throws ServiceException {
		return newsService.getPreviousId(filter, newsId);
	}

	@Override
	public Long addComment(CommentTO comment) throws ServiceException {
		return commentService.addComment(comment);
	}

	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		commentService.deleteComment(commentId);
	}

	@Override
	public void addNewTag(TagTO tag) throws ServiceException {
		tagService.addNewTag(tag);
	}

	@Override
	public List<AuthorTO> getAllAuthors() throws ServiceException {
		return authorService.getAll();
	}

	@Override
	public List<TagTO> getAllTags() throws ServiceException {
		return tagService.getAll();
	}

	@Override
	public void updateTag(TagTO tag) throws ServiceException {
		tagService.updateTag(tag);
	}

	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		tagService.deleteTag(tagId);
	}

	@Override
	public void updateAuthor(AuthorTO authorTO) throws ServiceException {
		authorService.updateAuthor(authorTO);
	}

	@Override
	public Long getPagesCount(Filter filter) throws ServiceException {
		return newsService.getPagesCount(filter);
	}

	private List<NewsVO> fillNewsVOList(List<NewsTO> newsTOlist)
			throws ServiceException {
		List<NewsVO> newsVOlist = new ArrayList<NewsVO>();
		for (NewsTO newsTO : newsTOlist) {
			NewsVO newsVO = new NewsVO();
			Long newsId = newsTO.getId();
			newsVO.setNews(newsTO);
			newsVO.setAuthor(authorService.getNewsAuthor(newsId));
			newsVO.setTags(tagService.getNewsTags(newsId));
			newsVO.setComments(commentService.getNewsComments(newsId));
			newsVOlist.add(newsVO);
		}
		return newsVOlist;
	}

	@Override
	public void deleteNews(Long newsId) throws ServiceException {
	}

	@Override
	public void expireAuthor(Long authorId) throws ServiceException {
		authorService.expireAuthor(authorId);
	}

	@Override
	public List<AuthorTO> getActualAuthors() throws ServiceException {
		return authorService.getActualAuthors();
	}
}