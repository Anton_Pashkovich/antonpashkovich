package com.epam.newsmanagementwebapp.service;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.entity.valueobject.NewsVO;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.util.Filter;

public interface INewsManagementService {
	
	void deleteNews(Long newsId) throws ServiceException;
	
	void editNews(NewsTO news, Long authorId, List<Long> tags) throws ServiceException;
	
	List<NewsVO> getAllNews() throws ServiceException;
	
	NewsVO getSingleNews(Long newsId) throws ServiceException;
	
	Long addNewsAuthor(AuthorTO author) throws ServiceException;
	
	List<NewsVO> getNewsByAuthor(Long authorId) throws ServiceException;
	
	Long addComment(CommentTO comment) throws ServiceException;
	
	void deleteComment(Long commentId) throws ServiceException;
	
	void addNewTag(TagTO tag) throws ServiceException;

	List<AuthorTO> getAllAuthors() throws ServiceException;
	
	List<TagTO> getAllTags() throws ServiceException;

	Long getNextId(Filter filter, Long newsId) throws ServiceException;

	Long getPreviousId(Filter filter, Long newsId) throws ServiceException;

	Long getPagesCount(Filter filter) throws ServiceException;

	List<NewsVO> getNewsByFilter(Filter filter, Long pageNum)
			throws ServiceException;

	void deleteNews(List<Long> newsIdList) throws ServiceException;

	Long addNews(NewsTO news, Long authorId, List<Long> tags)
			throws ServiceException;

	void deleteTag(Long tagId) throws ServiceException;

	void updateTag(TagTO tagTO) throws ServiceException;

	void updateAuthor(AuthorTO authorTO) throws ServiceException;

	void expireAuthor(Long authorId) throws ServiceException;

	List<AuthorTO> getActualAuthors() throws ServiceException;

}