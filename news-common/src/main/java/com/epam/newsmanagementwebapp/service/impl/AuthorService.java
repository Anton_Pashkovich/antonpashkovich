package com.epam.newsmanagementwebapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagementwebapp.dao.IAuthorDao;
import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.exception.DaoException;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.IAuthorService;

public class AuthorService implements IAuthorService{

	@Autowired
	private IAuthorDao authorDao;
	
	@Override
	public Long addNewAuthor(AuthorTO author) throws ServiceException {
		try {
			return authorDao.create(author);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void bindAuthorToNews(Long authorId, Long newsId) throws ServiceException {
		try {
			authorDao.bindAuthorToNews(authorId, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void unbindAuthorFromNews(Long newsId) throws ServiceException {
		try {
			authorDao.unbindAuthorFromNews(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public AuthorTO getNewsAuthor(Long newsId) throws ServiceException {
		try {
			return authorDao.getNewsAuthor(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	public List<AuthorTO> getAll() throws ServiceException {
		try {
			return authorDao.getAll();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateAuthor(AuthorTO authorTO) throws ServiceException {
		try {
			authorDao.update(authorTO);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void expireAuthor(Long authorId) throws ServiceException {
		try {
			authorDao.expireAuthor(authorId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<AuthorTO> getActualAuthors() throws ServiceException {
		try {
			return authorDao.getActualAuthors();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
}
