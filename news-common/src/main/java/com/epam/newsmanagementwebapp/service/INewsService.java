package com.epam.newsmanagementwebapp.service;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.util.Filter;

/**
 * Interface INewsService contains NewsService operations
 */
public interface INewsService {

	/**
	 * Method addNews creates a new news record
	 * 
	 * @param news
	 *            is a new news record
	 * @return created news id
	 * @throws ServiceException
	 */
	Long addNews(NewsTO news) throws ServiceException;

	/**
	 * Method editNews provides updating news data
	 * 
	 * @param news
	 *            is a news to update
	 * @throws ServiceException
	 */
	void editNews(NewsTO news) throws ServiceException;

	/**
	 * Method deleteNews deletes concrete news from databasw
	 * 
	 * @param newsId
	 *            is a news id to delete
	 * @throws ServiceException
	 */
	void deleteNews(Long newsId) throws ServiceException;

	/**
	 * Method getAllNews returns list of all news in database
	 * 
	 * @return list of all created news
	 * @throws ServiceException
	 */
	List<NewsTO> getAllNews() throws ServiceException;

	/**
	 * Method getSingleNews returns a single news from databade
	 * 
	 * @param newsId
	 *            is required news id
	 * @return required news
	 * @throws ServiceException
	 */
	NewsTO getSingleNews(Long newsId) throws ServiceException;

	/**
	 * Method getNewsByAuthor returns news list by concrete author
	 * 
	 * @param authorId
	 *            is required author id
	 * @return news list by concrete author
	 * @throws ServiceException
	 */
	List<NewsTO> getNewsByAuthor(Long authorId) throws ServiceException;

	Long getNextId(Filter filter, Long newsId) throws ServiceException;

	Long getPreviousId(Filter filter, Long newsId) throws ServiceException;

	Long getPagesCount(Filter filter) throws ServiceException;

	List<NewsTO> getNewsByFilter(Filter filter, Long pageNum)
			throws ServiceException;

	void deleteNews(List<Long> newsIdList) throws ServiceException;

}