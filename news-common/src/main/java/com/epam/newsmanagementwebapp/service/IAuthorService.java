package com.epam.newsmanagementwebapp.service;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.exception.ServiceException;

/**
 * Interface IAuthorService contains AuthorService operations
 */
public interface IAuthorService {

	/**
	 * Method addNewAuthor creates a new author record
	 * 
	 * @param author
	 *            is a new author
	 * @return created author's id
	 * @throws ServiceException
	 */
	Long addNewAuthor(AuthorTO author) throws ServiceException;

	/**
	 * Method bindAuthorToNews binds concrete author with some news
	 * 
	 * @param authorId
	 *            is appropriate author id
	 * @param newsId
	 *            is appropriate news id
	 * @throws ServiceException
	 */
	void bindAuthorToNews(Long authorId, Long newsId) throws ServiceException;

	/**
	 * Method unbindAuthorFromNews unbinds author from concrete news
	 * 
	 * @param newsId
	 *            is appropriate news id
	 * @throws ServiceException
	 */
	void unbindAuthorFromNews(Long newsId) throws ServiceException;

	/**
	 * Method getNewsAuthor returns concrete news author
	 * 
	 * @param newsId
	 *            is appropriate news id
	 * @return author entity of concrete news
	 * @throws ServiceException
	 */
	AuthorTO getNewsAuthor(Long newsId) throws ServiceException;

	List<AuthorTO> getAll() throws ServiceException;

	void updateAuthor(AuthorTO authorTO) throws ServiceException;

	void expireAuthor(Long authorId) throws ServiceException;

	List<AuthorTO> getActualAuthors() throws ServiceException;
}