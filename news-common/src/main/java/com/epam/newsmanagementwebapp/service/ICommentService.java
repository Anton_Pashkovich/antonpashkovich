package com.epam.newsmanagementwebapp.service;

import java.util.List;

import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.exception.ServiceException;

/**
 * Interface ICommentService contains CommentService operations
 */
public interface ICommentService {

	/**
	 * Method addComment creates a new comment record
	 * 
	 * @param comment
	 *            is a new comment
	 * @return created comment's id
	 * @throws ServiceException
	 */
	Long addComment(CommentTO comment) throws ServiceException;

	/**
	 * Method deleteComment deletes some comment from database
	 * 
	 * @param commentId
	 *            is appropriate comment id
	 * @throws ServiceException
	 */
	void deleteComment(Long commentId) throws ServiceException;

	/**
	 * Method getNewsComments returns all comments from appropriate news
	 * 
	 * @param newsId
	 *            is required news id
	 * @return list of appropriane news comments
	 * @throws ServiceException
	 */
	List<CommentTO> getNewsComments(Long newsId) throws ServiceException;
}