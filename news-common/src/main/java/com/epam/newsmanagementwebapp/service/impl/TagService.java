package com.epam.newsmanagementwebapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagementwebapp.dao.ITagDao;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;
import com.epam.newsmanagementwebapp.exception.DaoException;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.ITagService;

public class TagService implements ITagService {

	@Autowired
	private ITagDao tagDao;
	
	@Override
	public Long addNewTag(TagTO tag) throws ServiceException{
		try {
			return tagDao.create(tag);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void bindTagToNews(Long tagId, Long newsId) throws ServiceException{
		try {
			tagDao.bindTagToNews(tagId, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
	
	@Override
	public void bindTagsToNews(List<Long> tags, Long newsId) throws ServiceException {
		try {
			tagDao.bindTagsToNews(tags, newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void unbindAllTags(Long newsId) throws ServiceException {
		try {
			tagDao.unbindAllTags(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<TagTO> getNewsTags(Long newsId) throws ServiceException {
		try {
			return tagDao.getNewsTags(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<TagTO> getAll() throws ServiceException {
		try {
			return tagDao.getAll();
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteTag(Long tagId) throws ServiceException {
		try {
			tagDao.delete(tagId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void updateTag(TagTO tag) throws ServiceException {
		try {
			tagDao.update(tag);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

}