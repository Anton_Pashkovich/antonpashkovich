package com.epam.newsmanagementwebapp.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;

import com.epam.newsmanagementwebapp.dao.ICommentDao;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.exception.DaoException;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.ICommentService;

public class CommentService implements ICommentService{

	@Autowired
	private ICommentDao commentDao;
	
	@Override
	public Long addComment(CommentTO comment) throws ServiceException {
		try {
			return commentDao.create(comment);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public void deleteComment(Long commentId) throws ServiceException {
		try {
			commentDao.delete(commentId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}

	@Override
	public List<CommentTO> getNewsComments(Long newsId) throws ServiceException {
		try {
			return commentDao.getNewsComments(newsId);
		} catch (DaoException e) {
			throw new ServiceException(e);
		}
	}
}