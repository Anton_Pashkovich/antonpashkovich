package com.epam.newsmanagementwebapp.entity.transferobject;

import java.io.Serializable;
import java.util.Date;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity corresponding row of the table COMMENTS.
 */

@Entity
@Table(name = "COMMENTS")
public class CommentTO implements Serializable {
	private static final long serialVersionUID = -8367417695346129957L;

	@Id
	@SequenceGenerator(name="COMMENT_ID_SEQUENCE", sequenceName="COMMENT_ID_SEQ", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "COMMENT_ID_SEQUENCE")
	@Column(name = "COMMENT_ID")
	private Long id;

	@Column(name = "COMMENT_TEXT")
	private String commentText;

	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Column(name = "NEWS_ID")
	private Long newsId;

	public CommentTO() {}
	
	public CommentTO(Long id, Long newsId, String commentText, Date creationDate) {
		this.id = id;
		this.commentText = commentText;
		this.creationDate = creationDate;
		this.newsId = newsId;
	}

	public CommentTO(Long newsId, String text, Date date) {
		this.commentText = text;
		this.creationDate = date;
		this.newsId = newsId;
	}

	public String getCommentText() {
		return commentText;
	}

	public void setCommentText(String text) {
		this.commentText = text;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Long getNewsId() {
		return newsId;
	}

	public void setNewsId(Long newsId) {
		this.newsId = newsId;
	}

	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (null == object) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}

		CommentTO comment = (CommentTO) object;

		if (!commentText.equals(comment.getCommentText())) {
			return false;
		}
		if (!creationDate.equals(comment.getCreationDate())) {
			return false;
		}
		if (!newsId.equals(comment.getNewsId())) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		int result = 17;
		result += super.hashCode();
		result += (commentText != null ? commentText.hashCode() : 0);
		result += (creationDate != null ? creationDate.hashCode() : 0);
		result += (newsId != null ? newsId.hashCode() : 0);
		return result;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(this.getClass().getName());
		result.append("{");
		result.append(" id = ").append(getId());
		result.append(" text = ").append(commentText);
		result.append(" creationDate = ").append(creationDate);
		result.append(" newsId = ").append(newsId);
		result.append("}");
		return result.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

}
