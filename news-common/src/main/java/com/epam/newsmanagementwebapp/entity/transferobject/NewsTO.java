package com.epam.newsmanagementwebapp.entity.transferobject;

import java.io.Serializable;
import java.util.Collection;
import java.util.Date;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import javax.persistence.Version;

/**
 * Entity corresponding row of the table NEWS.
 */

@Entity
@Table(name = "NEWS", uniqueConstraints = { @UniqueConstraint(columnNames = { "NEWS_ID" }) })
public class NewsTO implements Serializable {

	private static final long serialVersionUID = -997280634354664090L;

	@Id
	@SequenceGenerator(name = "NEWS_ID_SEQUENCE", sequenceName = "NEWS_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "NEWS_ID_SEQUENCE")
	@Column(name = "NEWS_ID")
	private Long id;

	@Column(name = "SHORT_TEXT")
	private String shortText;

	@Column(name = "FULL_TEXT")
	private String fullText;

	@Column(name = "TITLE")
	private String title;

	@Column(name = "CREATION_DATE")
	private Date creationDate;

	@Column(name = "MODIFICATION_DATE")
	private Date modificationDate;

	@Version
	private Long version;

	@Version
	public Long getVersion() {
		return version;
	}

	public void setVersion(Long version) {
		this.version = version;
	}

	@OneToMany
	@JoinColumn(name = "NEWS_ID", insertable = false, updatable = false)
	private List<CommentTO> commentTO;

	@ManyToMany
	@JoinTable(name = "NEWS_TAG", 
	joinColumns = { @JoinColumn(name = "NEWS_ID", insertable = false, updatable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "TAG_ID", insertable = false, updatable = false) })
	private List<TagTO> tagTO;

	@ManyToMany
	@JoinTable(name = "NEWS_AUTHOR", 
	joinColumns = { @JoinColumn(name = "NEWS_ID", insertable = false, updatable = false)}, 
	inverseJoinColumns = { @JoinColumn(name = "AUTHOR_ID", insertable = false, updatable = false)})
	private List<AuthorTO> authorTO;

	public NewsTO() {}
	
	public NewsTO(Long id, String shortText, String fullText, String title,
			Date creationDate, Date modificationDate) {
		this.id = id;
		this.shortText = shortText;
		this.fullText = fullText;
		this.title = title;
		this.creationDate = creationDate;
		this.modificationDate = modificationDate;
	}

	public String getShortText() {
		return shortText;
	}

	public void setShortText(String shortText) {
		this.shortText = shortText;
	}

	public String getFullText() {
		return fullText;
	}

	public void setFullText(String fullText) {
		this.fullText = fullText;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public Date getCreationDate() {
		return creationDate;
	}

	public void setCreationDate(Date creationDate) {
		this.creationDate = creationDate;
	}

	public Date getModificationDate() {
		return modificationDate;
	}

	public void setModificationDate(Date modificationDate) {
		this.modificationDate = modificationDate;
	}

	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (null == object) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}

		NewsTO newsMessage = (NewsTO) object;

		if (!creationDate.equals(newsMessage.getCreationDate())) {
			return false;
		}
		if (!fullText.equals(newsMessage.getFullText())) {
			return false;
		}
		if (!modificationDate.equals(newsMessage.getModificationDate())) {
			return false;
		}
		if (!shortText.equals(newsMessage.getShortText())) {
			return false;
		}
		if (!title.equals(newsMessage.getTitle())) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		int result = 17;
		result += super.hashCode();
		result += (creationDate != null ? creationDate.hashCode() : 0);
		result += (fullText != null ? fullText.hashCode() : 0);
		result += (modificationDate != null ? modificationDate.hashCode() : 0);
		result += (shortText != null ? shortText.hashCode() : 0);
		result += (title != null ? title.hashCode() : 0);
		return result;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(this.getClass().getName());
		result.append("{");
		result.append(" id = ").append(getId());
		result.append(" shortText = ").append(shortText);
		result.append(" fullText = ").append(fullText);
		result.append(" title = ").append(title);
		result.append(" creationDate = ").append(creationDate);
		result.append(" modificationDate = ").append(modificationDate);
		result.append(" version = ").append(version);
		result.append("}");
		return result.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<CommentTO> getCommentTO() {
	    return commentTO;
	}

	public void setCommentTO(List<CommentTO> param) {
	    this.commentTO = param;
	}

	public Collection<TagTO> getTagTO() {
	    return tagTO;
	}

	public void setTagTO(List<TagTO> param) {
	    this.tagTO = param;
	}

	public Collection<AuthorTO> getAuthorTO() {
	    return authorTO;
	}

	public void setAuthorTO(List<AuthorTO> param) {
	    this.authorTO = param;
	}
}
