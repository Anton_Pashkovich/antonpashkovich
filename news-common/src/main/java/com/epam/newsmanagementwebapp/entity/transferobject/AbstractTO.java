package com.epam.newsmanagementwebapp.entity.transferobject;

import java.io.Serializable;

/**
 * Abstract entity contains id only
 */
public abstract class AbstractTO implements Serializable {

	private static final long serialVersionUID = 7216452703283010798L;
	protected Long id;

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + (int) (id ^ (id >>> 32));
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		AbstractTO other = (AbstractTO) obj;
		return id == other.id;
	}
}
