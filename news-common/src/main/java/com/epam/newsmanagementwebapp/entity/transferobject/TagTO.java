package com.epam.newsmanagementwebapp.entity.transferobject;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import java.util.Collection;

/**
 * Entity corresponding row of the table TAG.
 *
 */

@Entity
@Table(name = "TAG", uniqueConstraints = { @UniqueConstraint(columnNames = { "TAG_ID" }) })
public class TagTO implements Serializable {
	private static final long serialVersionUID = -723471917083829649L;

	@Id
	@SequenceGenerator(name = "TAG_ID_SEQUENCE", sequenceName = "TAG_ID_SEQ", allocationSize = 1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "TAG_ID_SEQUENCE")
	@Column(name = "TAG_ID")
	private Long id;

	@Column(name = "TAG_NAME")
	private String name;

	@ManyToMany
	@JoinTable(name = "NEWS_TAG", 
	joinColumns = { @JoinColumn(name = "TAG_ID", updatable = false, insertable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "NEWS_ID", updatable = false, insertable = false) })
	private List<NewsTO> newsTO;

	

	public TagTO() {}
	
	public TagTO(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}
		if (null == object) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}
		TagTO tag = (TagTO) object;

		if (!name.equals(tag.getName())) {
			return false;
		}
		return true;
	}

	public int hashCode() {
		return (name != null ? name.hashCode() : 0);
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(this.getClass().getName());
		result.append("{");
		result.append(" id = ").append(getId());
		result.append(" name = ").append(name);
		result.append("}");
		return result.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public Collection<NewsTO> getNewsTO() {
	    return newsTO;
	}

	public void setNewsTO(List<NewsTO> param) {
	    this.newsTO = param;
	}

}
