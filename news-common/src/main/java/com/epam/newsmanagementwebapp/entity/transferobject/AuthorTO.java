package com.epam.newsmanagementwebapp.entity.transferobject;

import java.io.Serializable;
import java.util.List;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.SequenceGenerator;
import javax.persistence.Table;

/**
 * Entity corresponding row of the table AUTHOR.
 */
@Entity
@Table(name = "AUTHOR")
public class AuthorTO implements Serializable {
	private static final long serialVersionUID = 5431696578972056246L;

	@Id
	@SequenceGenerator(name="AUTHOR_ID_SEQUENCE", sequenceName="AUTHOR_ID_SEQ", allocationSize=1)
	@GeneratedValue(strategy = GenerationType.SEQUENCE, generator = "AUTHOR_ID_SEQUENCE")
	@Column(name = "AUTHOR_ID")
	private Long id;

	@Column(name = "NAME")
	private String name;

	@ManyToMany
	@JoinTable(name = "NEWS_AUTHOR", 
	joinColumns = { @JoinColumn(name = "AUTHOR_ID", updatable = false, insertable = false) }, 
	inverseJoinColumns = { @JoinColumn(name = "NEWS_ID", updatable = false, insertable = false) })
	private List<NewsTO> newsTO;
	
	public AuthorTO() {}
	
	public AuthorTO(Long id, String name) {
		this.id = id;
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public boolean equals(Object object) {
		if (this == object) {
			return true;
		}

		if (null == object) {
			return false;
		}
		if (getClass() != object.getClass()) {
			return false;
		}
		AuthorTO author = (AuthorTO) object;

		if (!name.equals(author.getName())) {
			return false;
		}

		return true;
	}

	public int hashCode() {
		int result = 17;
		result += super.hashCode();
		result += (name != null ? name.hashCode() : 0);
		return result;
	}

	public String toString() {
		StringBuilder result = new StringBuilder();
		result.append(this.getClass().getName());
		result.append("{");
		result.append(" id = ").append(getId());
		result.append(", name = ").append(name);
		result.append("}");
		return result.toString();
	}

	public Long getId() {
		return id;
	}

	public void setId(Long id) {
		this.id = id;
	}

	public List<NewsTO> getNewsTO() {
	    return newsTO;
	}

	public void setNewsTO(List<NewsTO> param) {
	    this.newsTO = param;
	}

}
