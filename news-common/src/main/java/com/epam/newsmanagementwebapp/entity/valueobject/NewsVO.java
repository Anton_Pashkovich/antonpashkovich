package com.epam.newsmanagementwebapp.entity.valueobject;

import java.util.ArrayList;
import java.util.List;
import java.util.Objects;

import com.epam.newsmanagementwebapp.entity.transferobject.AuthorTO;
import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.entity.transferobject.NewsTO;
import com.epam.newsmanagementwebapp.entity.transferobject.TagTO;

/**
 * Class NewsVO represents the entity of news(includes news, author comment list
 * and tags)
 */
public class NewsVO {
	private NewsTO news;
	private AuthorTO author;
	private List<CommentTO> comments;
	private List<TagTO> tags;

	public NewsTO getNews() {
		return news;
	}

	public void setNews(NewsTO news) {
		if (news != null) {
			this.news = news;
		}
	}

	public AuthorTO getAuthor() {
		return author;
	}

	public void setAuthor(AuthorTO author) {
		if (author != null) {
			this.author = author;
		}
	}

	public List<CommentTO> getComments() {
		return comments;
	}

	public void setComments(List<CommentTO> comments) {
		if (comments != null) {
			this.comments = comments;
		}
	}

	public List<TagTO> getTags() {
		return tags;
	}

	public void setTags(List<TagTO> tags) {
		if (tags != null) {
			this.tags = tags;
		}
	}

	@Override
	public int hashCode() {
		return Objects.hash(news, author, comments, tags);
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj) {
			return true;
		}
		if (obj == null) {
			return false;
		}
		if (getClass() != obj.getClass()) {
			return false;
		}
		NewsVO other = (NewsVO) obj;
		return news.equals(other.news) && author.equals(other.author)
				&& comments.equals(other.comments) && tags.equals(other.tags);
	}
	
	public List<Long> getTagIdList() {
		List<Long> tagIdList = new ArrayList<Long>();
		for (TagTO tag: this.getTags()) {
			tagIdList.add(tag.getId());
		}
		return tagIdList;
	}
}
