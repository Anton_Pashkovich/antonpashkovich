<%@ page language="java" contentType="text/html; UTF-8"
	pageEncoding="UTF-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<%@ taglib uri="http://www.springframework.org/tags/form" prefix="form"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/functions" prefix="fn"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/news-list.css" />
<script
	src="${pageContext.request.contextPath}/resources/js/jquery.min.js"></script>
</head>

<div class="body">
	<form action="${pageContext.request.contextPath}/news/byFilter"
		method="post">
		<div class="filter">
			<div class="multiselect">
				<select name="authorId" size="1">
					<c:choose>
						<c:when test="${not empty filter.author}">
							<c:forEach var="author" items="${authorsList}">
								<c:if test="${author.id == filter.author}">
									<option value="">
										<spring:message code="label.select.author" />
									</option>
									<option selected="selected" value="${filter.author}">
										<c:out value="${author.name}" />
									</option>
								</c:if>
							</c:forEach>
						</c:when>
						<c:otherwise>
							<option selected="selected" disabled>
								<spring:message code="label.select.author" />
							</option>
						</c:otherwise>
					</c:choose>
					<c:forEach var="author" items="${authorsList}">
						<c:if test="${author.id != filter.author}">
							<option value="${author.id}" style="color: black;">
								<c:out value="${author.name}" />
							</option>
						</c:if>
					</c:forEach>
				</select>
			</div>
			<div class="multiselect">
				<div class="select_box" onclick="showCheckboxes()">
					<select>
						<option selected="selected" value="false"><spring:message
								code="label.select.tags" /></option>
					</select>
					<div class="over_select" id="over_select"></div>
				</div>
				<div id="checkboxes">
					<c:forEach var="tag" items="${tagsList}">
						<label for="${tag.name}"> <c:set var="checked" value="no" />
							<c:forEach var="filtersTagId" items="${filter.tags}">
								<c:if test="${tag.id == filtersTagId}">
									<c:set var="checked" value="yes" />
								</c:if>
							</c:forEach> <c:if test="${checked == 'yes'}">
								<input type="checkbox" name="selectedTags" value="${tag.id}"
									checked="checked" />
							</c:if> <c:if test="${checked == 'no'}">
								<input type="checkbox" name="selectedTags" value="${tag.id}" />
							</c:if> <span><c:out value="${tag.name}" /></span>
						</label>
					</c:forEach>
				</div>
			</div>
			<div class="filter_button_area">
				<input type="submit" class="filter_button_style" name="filter"
					value="<spring:message code="label.filter" />" />
			</div>
		</div>
	</form>
	<form action="${pageContext.request.contextPath}/news/reset"
		method="GET">
		<div class="reset_button_area">
			<input type="submit" class="filter_button_style" name="reset"
				value="<spring:message code="label.reset" />" />
		</div>
	</form>

	<c:choose>
		<c:when test="${empty newsList}">
			<div class="message">
				<spring:message code="label.no.news" />
			</div>
		</c:when>
		<c:otherwise>
			<c:forEach var="concreteNews" items="${newsList}">
				<div class="newslist">
					<div class="news_title_author_date">
						<div class="title">
							<c:out value="${concreteNews.news.title}" />
						</div>
						<div class="author">
							<c:out value="( by ${concreteNews.author.name} )" />
						</div>
						<span> <fmt:formatDate
								value="${concreteNews.news.modificationDate}" />
						</span>
					</div>

					<div class="brief">
						<c:out value="${concreteNews.news.shortText}" />
					</div>

					<div class="news_tags_comments">
						<div class="links">
							<c:url var="view" value="/news/${concreteNews.news.id}/view" />
							<a href="${view}"><spring:message code="label.view" /></a>
						</div>
						<div class="comments">
							<spring:message code="label.comments" />
							<c:out value="(${fn:length(concreteNews.comments)})" />
						</div>
						<div class="tag">
							<c:forEach var="tag" items="${concreteNews.tags}">
								<c:out value="${tag.name}  " />
							</c:forEach>
						</div>
					</div>
				</div>
			</c:forEach>
		</c:otherwise>
	</c:choose>

	<div class="paging">
		<div class="page_numbers">
			<c:forEach var="i" begin="1" end="${pagesCnt}">
				<c:url var="page" value="/news/page/${i}" />
				<c:choose>
					<c:when test="${pageNum eq i}">
						<a href="${page}">
							<button style="background-color: gray; color: white;">${i}</button>
						</a>
					</c:when>
					<c:otherwise>
						<a href="${page}">
							<button>${i}</button>
						</a>
					</c:otherwise>
				</c:choose>
			</c:forEach>
		</div>
	</div>
</div>

<script
	src="${pageContext.request.contextPath}/resources/js/dropdown.js"></script>
<script
	src="${pageContext.request.contextPath}/resources/js/showCheckboxes.js"></script>