<%@ taglib uri="http://www.springframework.org/tags" prefix="spring"%>
<head>
<link rel="stylesheet" type="text/css"
	href="${pageContext.request.contextPath}/resources/css/error.css" />
</head>
<div class="body">
	<div class="errorMessage">
		<spring:message code="label.error.message" />
	</div>
</div>