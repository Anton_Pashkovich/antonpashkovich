<%@ page language="java" contentType="text/html; charset=utf-8"
	pageEncoding="utf-8"%>
<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<div class="header">
	<div class="header_title">News Portal</div>
	<div class="localization">
		<span> <c:url var="index" value="/news/changeLocale" /> <a
			href="?lang=en">EN</a> <a href="?lang=ru">RU</a>
		</span>
	</div>
</div>