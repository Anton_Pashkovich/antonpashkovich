package com.epam.newsmanagementwebapp.controller;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.ModelAttribute;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.SessionAttributes;

import com.epam.newsmanagementwebapp.entity.transferobject.CommentTO;
import com.epam.newsmanagementwebapp.exception.ServiceException;
import com.epam.newsmanagementwebapp.service.INewsManagementService;
import com.epam.newsmanagementwebapp.util.Filter;

@Controller
@SessionAttributes({ "filter", "pageNum" })
public class NewsController {
	@Autowired
	private INewsManagementService newsManagementService;

	@RequestMapping("/")
	public String main(Model model, HttpServletRequest request) {
		model.addAttribute("pageNum", 1L);
		model.addAttribute("filter", new Filter());
		return "forward:/news";
	}

	@RequestMapping("/news")
	public String index(Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		Long pageNum = (Long) request.getSession().getAttribute("pageNum");
		if (pageNum == null) {
			pageNum = 1L;
		}
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, pageNum));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
			model.addAttribute("pagesCnt",
					newsManagementService.getPagesCount(filter));
		} catch (ServiceException e) {
			return "error";
		}
		model.addAttribute("pageNum", pageNum);
		return "index";
	}

	@RequestMapping("/news/byFilter")
	public String setFilter(
			@RequestParam(value = "authorId", required = false) Long authorId,
			@RequestParam(value = "selectedTags", required = false) List<Long> selectedIdTags,
			Model model) {
		Filter filter = new Filter();
		if (authorId != null) {
			filter.setAuthor(authorId);
		}
		if (selectedIdTags != null) {
			List<Long> selectedTagList = new ArrayList<Long>();
			for (Long tagId : selectedIdTags) {
				selectedTagList.add(tagId);
			}
			filter.setTags(selectedTagList);
		}
		model.addAttribute("pageNum", 1L);
		model.addAttribute("filter", filter);
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, 1L));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:";
	}

	@RequestMapping("news/reset")
	public String resetFilter(Model model) {
		model.addAttribute("filter", new Filter());
		model.addAttribute("pageNum", 1L);
		return "redirect:";
	}

	@RequestMapping("news/page/{pageNum}")
	public String setPage(@PathVariable(value = "pageNum") Long pageNum,
			Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		model.addAttribute("pageNum", pageNum);
		try {
			model.addAttribute("newsList",
					newsManagementService.getNewsByFilter(filter, pageNum));
			model.addAttribute("authorsList",
					newsManagementService.getAllAuthors());
			model.addAttribute("tagsList", newsManagementService.getAllTags());
			model.addAttribute("pagesCnt",
					newsManagementService.getPagesCount(filter));
		} catch (ServiceException e) {
			return "error";
		}
		return "index";
	}

	@RequestMapping("/news/{newsId}/view")
	public String newsPage(@PathVariable(value = "newsId") Long newsId,
			Model model, HttpServletRequest request) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		try {
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (ServiceException e) {
			return "error";
		}
		return "view";
	}

	@RequestMapping("/news/{newsId}/postComment")
	public String postComment(HttpServletRequest request,
			@PathVariable(value = "newsId") Long newsId,
			@ModelAttribute("commentText") String text, Model model) {
		Filter filter = (Filter) request.getSession().getAttribute("filter");
		Date date = new Date();
		try {
			newsManagementService.addComment(new CommentTO(newsId, text, date));
			model.addAttribute("news",
					newsManagementService.getSingleNews(newsId));
			model.addAttribute("nextId",
					newsManagementService.getNextId(filter, newsId));
			model.addAttribute("prevId",
					newsManagementService.getPreviousId(filter, newsId));
		} catch (ServiceException e) {
			return "error";
		}
		return "redirect:/news/{newsId}/view";
	}
}